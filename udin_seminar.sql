-- MySQL dump 10.16  Distrib 10.1.44-MariaDB, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: udin_seminar
-- ------------------------------------------------------
-- Server version	10.1.44-MariaDB-0ubuntu0.18.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `authors`
--

DROP TABLE IF EXISTS `authors`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `authors` (
  `authors_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `authors_first_name` varchar(255) DEFAULT NULL,
  `authors_last_name` varchar(255) DEFAULT NULL,
  `authors_email` varchar(255) DEFAULT NULL,
  `authors_country` int(10) unsigned DEFAULT NULL,
  `authors_address` varchar(255) DEFAULT NULL,
  `authors_phone_number` char(20) DEFAULT NULL,
  `authors_organization` varchar(255) DEFAULT NULL,
  `register_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `is_delete` int(11) DEFAULT NULL,
  `authors_type` int(11) DEFAULT NULL,
  `authors_student_number` char(20) DEFAULT NULL,
  `authors_period` int(10) unsigned DEFAULT NULL,
  `authors_regis_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`authors_id`),
  KEY `authors_country` (`authors_country`),
  KEY `authors_period` (`authors_period`),
  KEY `authors_regis_id` (`authors_regis_id`),
  CONSTRAINT `authors_ibfk_1` FOREIGN KEY (`authors_country`) REFERENCES `country` (`country_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `authors_ibfk_2` FOREIGN KEY (`authors_period`) REFERENCES `periode` (`periode_id`),
  CONSTRAINT `authors_ibfk_3` FOREIGN KEY (`authors_regis_id`) REFERENCES `temp_regis` (`regis_id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `authors`
--

LOCK TABLES `authors` WRITE;
/*!40000 ALTER TABLE `authors` DISABLE KEYS */;
INSERT INTO `authors` VALUES (17,'Jacky ','Lucky','aidenstacks3@gmail.com',102,'Gedangan Kulon Wetan','6354789214','ITATS','2020-08-12 10:51:10',0,2,'14.2018.1.00987',1,11);
/*!40000 ALTER TABLE `authors` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `base_settings`
--

DROP TABLE IF EXISTS `base_settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `base_settings` (
  `base_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `base_company_name` varchar(255) DEFAULT NULL,
  `base_dashboard_color` varchar(255) DEFAULT NULL,
  `base_contact_us` varchar(255) DEFAULT NULL,
  `is_active` int(11) DEFAULT NULL,
  `is_delete` int(11) DEFAULT NULL,
  `base_email` varchar(255) DEFAULT NULL,
  `base_email_password` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`base_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `base_settings`
--

LOCK TABLES `base_settings` WRITE;
/*!40000 ALTER TABLE `base_settings` DISABLE KEYS */;
INSERT INTO `base_settings` VALUES (1,'ICATECH','','<p>ICATECH</p>\r\n',0,0,'valhein080899@gmail.com','graha080899');
/*!40000 ALTER TABLE `base_settings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `country`
--

DROP TABLE IF EXISTS `country`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `country` (
  `country_id` int(5) unsigned NOT NULL AUTO_INCREMENT,
  `country_code` char(5) NOT NULL,
  `country_name` varchar(100) NOT NULL,
  PRIMARY KEY (`country_id`)
) ENGINE=InnoDB AUTO_INCREMENT=247 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `country`
--

LOCK TABLES `country` WRITE;
/*!40000 ALTER TABLE `country` DISABLE KEYS */;
INSERT INTO `country` VALUES (1,'AF','Afghanistan'),(2,'AL','Albania'),(3,'DZ','Algeria'),(4,'DS','American Samoa'),(5,'AD','Andorra'),(6,'AO','Angola'),(7,'AI','Anguilla'),(8,'AQ','Antarctica'),(9,'AG','Antigua and Barbuda'),(10,'AR','Argentina'),(11,'AM','Armenia'),(12,'AW','Aruba'),(13,'AU','Australia'),(14,'AT','Austria'),(15,'AZ','Azerbaijan'),(16,'BS','Bahamas'),(17,'BH','Bahrain'),(18,'BD','Bangladesh'),(19,'BB','Barbados'),(20,'BY','Belarus'),(21,'BE','Belgium'),(22,'BZ','Belize'),(23,'BJ','Benin'),(24,'BM','Bermuda'),(25,'BT','Bhutan'),(26,'BO','Bolivia'),(27,'BA','Bosnia and Herzegovina'),(28,'BW','Botswana'),(29,'BV','Bouvet Island'),(30,'BR','Brazil'),(31,'IO','British Indian Ocean Territory'),(32,'BN','Brunei Darussalam'),(33,'BG','Bulgaria'),(34,'BF','Burkina Faso'),(35,'BI','Burundi'),(36,'KH','Cambodia'),(37,'CM','Cameroon'),(38,'CA','Canada'),(39,'CV','Cape Verde'),(40,'KY','Cayman Islands'),(41,'CF','Central African Republic'),(42,'TD','Chad'),(43,'CL','Chile'),(44,'CN','China'),(45,'CX','Christmas Island'),(46,'CC','Cocos (Keeling) Islands'),(47,'CO','Colombia'),(48,'KM','Comoros'),(49,'CD','Democratic Republic of the Congo'),(50,'CG','Republic of Congo'),(51,'CK','Cook Islands'),(52,'CR','Costa Rica'),(53,'HR','Croatia (Hrvatska)'),(54,'CU','Cuba'),(55,'CY','Cyprus'),(56,'CZ','Czech Republic'),(57,'DK','Denmark'),(58,'DJ','Djibouti'),(59,'DM','Dominica'),(60,'DO','Dominican Republic'),(61,'TP','East Timor'),(62,'EC','Ecuador'),(63,'EG','Egypt'),(64,'SV','El Salvador'),(65,'GQ','Equatorial Guinea'),(66,'ER','Eritrea'),(67,'EE','Estonia'),(68,'ET','Ethiopia'),(69,'FK','Falkland Islands (Malvinas)'),(70,'FO','Faroe Islands'),(71,'FJ','Fiji'),(72,'FI','Finland'),(73,'FR','France'),(74,'FX','France, Metropolitan'),(75,'GF','French Guiana'),(76,'PF','French Polynesia'),(77,'TF','French Southern Territories'),(78,'GA','Gabon'),(79,'GM','Gambia'),(80,'GE','Georgia'),(81,'DE','Germany'),(82,'GH','Ghana'),(83,'GI','Gibraltar'),(84,'GK','Guernsey'),(85,'GR','Greece'),(86,'GL','Greenland'),(87,'GD','Grenada'),(88,'GP','Guadeloupe'),(89,'GU','Guam'),(90,'GT','Guatemala'),(91,'GN','Guinea'),(92,'GW','Guinea-Bissau'),(93,'GY','Guyana'),(94,'HT','Haiti'),(95,'HM','Heard and Mc Donald Islands'),(96,'HN','Honduras'),(97,'HK','Hong Kong'),(98,'HU','Hungary'),(99,'IS','Iceland'),(100,'IN','India'),(101,'IM','Isle of Man'),(102,'ID','Indonesia'),(103,'IR','Iran (Islamic Republic of)'),(104,'IQ','Iraq'),(105,'IE','Ireland'),(106,'IL','Israel'),(107,'IT','Italy'),(108,'CI','Ivory Coast'),(109,'JE','Jersey'),(110,'JM','Jamaica'),(111,'JP','Japan'),(112,'JO','Jordan'),(113,'KZ','Kazakhstan'),(114,'KE','Kenya'),(115,'KI','Kiribati'),(116,'KP','Korea, Democratic People\'s Republic of'),(117,'KR','Korea, Republic of'),(118,'XK','Kosovo'),(119,'KW','Kuwait'),(120,'KG','Kyrgyzstan'),(121,'LA','Lao People\'s Democratic Republic'),(122,'LV','Latvia'),(123,'LB','Lebanon'),(124,'LS','Lesotho'),(125,'LR','Liberia'),(126,'LY','Libyan Arab Jamahiriya'),(127,'LI','Liechtenstein'),(128,'LT','Lithuania'),(129,'LU','Luxembourg'),(130,'MO','Macau'),(131,'MK','North Macedonia'),(132,'MG','Madagascar'),(133,'MW','Malawi'),(134,'MY','Malaysia'),(135,'MV','Maldives'),(136,'ML','Mali'),(137,'MT','Malta'),(138,'MH','Marshall Islands'),(139,'MQ','Martinique'),(140,'MR','Mauritania'),(141,'MU','Mauritius'),(142,'TY','Mayotte'),(143,'MX','Mexico'),(144,'FM','Micronesia, Federated States of'),(145,'MD','Moldova, Republic of'),(146,'MC','Monaco'),(147,'MN','Mongolia'),(148,'ME','Montenegro'),(149,'MS','Montserrat'),(150,'MA','Morocco'),(151,'MZ','Mozambique'),(152,'MM','Myanmar'),(153,'NA','Namibia'),(154,'NR','Nauru'),(155,'NP','Nepal'),(156,'NL','Netherlands'),(157,'AN','Netherlands Antilles'),(158,'NC','New Caledonia'),(159,'NZ','New Zealand'),(160,'NI','Nicaragua'),(161,'NE','Niger'),(162,'NG','Nigeria'),(163,'NU','Niue'),(164,'NF','Norfolk Island'),(165,'MP','Northern Mariana Islands'),(166,'NO','Norway'),(167,'OM','Oman'),(168,'PK','Pakistan'),(169,'PW','Palau'),(170,'PS','Palestine'),(171,'PA','Panama'),(172,'PG','Papua New Guinea'),(173,'PY','Paraguay'),(174,'PE','Peru'),(175,'PH','Philippines'),(176,'PN','Pitcairn'),(177,'PL','Poland'),(178,'PT','Portugal'),(179,'PR','Puerto Rico'),(180,'QA','Qatar'),(181,'RE','Reunion'),(182,'RO','Romania'),(183,'RU','Russian Federation'),(184,'RW','Rwanda'),(185,'KN','Saint Kitts and Nevis'),(186,'LC','Saint Lucia'),(187,'VC','Saint Vincent and the Grenadines'),(188,'WS','Samoa'),(189,'SM','San Marino'),(190,'ST','Sao Tome and Principe'),(191,'SA','Saudi Arabia'),(192,'SN','Senegal'),(193,'RS','Serbia'),(194,'SC','Seychelles'),(195,'SL','Sierra Leone'),(196,'SG','Singapore'),(197,'SK','Slovakia'),(198,'SI','Slovenia'),(199,'SB','Solomon Islands'),(200,'SO','Somalia'),(201,'ZA','South Africa'),(202,'GS','South Georgia South Sandwich Islands'),(203,'SS','South Sudan'),(204,'ES','Spain'),(205,'LK','Sri Lanka'),(206,'SH','St. Helena'),(207,'PM','St. Pierre and Miquelon'),(208,'SD','Sudan'),(209,'SR','Suriname'),(210,'SJ','Svalbard and Jan Mayen Islands'),(211,'SZ','Swaziland'),(212,'SE','Sweden'),(213,'CH','Switzerland'),(214,'SY','Syrian Arab Republic'),(215,'TW','Taiwan'),(216,'TJ','Tajikistan'),(217,'TZ','Tanzania, United Republic of'),(218,'TH','Thailand'),(219,'TG','Togo'),(220,'TK','Tokelau'),(221,'TO','Tonga'),(222,'TT','Trinidad and Tobago'),(223,'TN','Tunisia'),(224,'TR','Turkey'),(225,'TM','Turkmenistan'),(226,'TC','Turks and Caicos Islands'),(227,'TV','Tuvalu'),(228,'UG','Uganda'),(229,'UA','Ukraine'),(230,'AE','United Arab Emirates'),(231,'GB','United Kingdom'),(232,'US','United States'),(233,'UM','United States minor outlying islands'),(234,'UY','Uruguay'),(235,'UZ','Uzbekistan'),(236,'VU','Vanuatu'),(237,'VA','Vatican City State'),(238,'VE','Venezuela'),(239,'VN','Vietnam'),(240,'VG','Virgin Islands (British)'),(241,'VI','Virgin Islands (U.S.)'),(242,'WF','Wallis and Futuna Islands'),(243,'EH','Western Sahara'),(244,'YE','Yemen'),(245,'ZM','Zambia'),(246,'ZW','Zimbabwe');
/*!40000 ALTER TABLE `country` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `front_about_countdown`
--

DROP TABLE IF EXISTS `front_about_countdown`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `front_about_countdown` (
  `front_about_countdown_id` int(11) NOT NULL AUTO_INCREMENT,
  `front_about_countdown_title` varchar(255) DEFAULT NULL,
  `front_about_countdown_content` text,
  `front_about_countdown_content_image` varchar(255) DEFAULT NULL,
  `is_delete` int(11) DEFAULT NULL,
  `is_active` int(11) DEFAULT NULL,
  PRIMARY KEY (`front_about_countdown_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `front_about_countdown`
--

LOCK TABLES `front_about_countdown` WRITE;
/*!40000 ALTER TABLE `front_about_countdown` DISABLE KEYS */;
INSERT INTO `front_about_countdown` VALUES (1,'Contact','<div class=\"footer-contact-info\">\r\n<p><a href=\"https://www.google.com/maps/place/Institut+Teknologi+Adhi+Tama+Surabaya/@-7.2900495,112.7790768,15z/data=!4m5!3m4!1s0x0:0x8d971ebe4e20a0d5!8m2!3d-7.2900495!4d112.7790768\" target=\"_blank\">Jl. Arief Rahman Hakim 100, Surabaya</a></p>\r\n\r\n<p><a href=\"tel:6282110002280\">+62 821 1000 2280</a></p>\r\n\r\n<p><a href=\"icatech.support@itats.ac.id\" target=\"_blank\">icatech.support@itats.ac.id</a></p>\r\n</div>\r\n',NULL,0,1),(2,'Contact','<p><a style = \"color:black;\" href=\"https://www.google.com/maps/place/Institut+Teknologi+Adhi+Tama+Surabaya/@-7.2900495,112.7790768,15z/data=!4m5!3m4!1s0x0:0x8d971ebe4e20a0d5!8m2!3d-7.2900495!4d112.7790768\" target=\"_blank\">Jl. Arief Rahman Hakim 100, Surabaya</a></p>\r\n\r\n<p><a style = \"color:black;\" href=\"tel:6282110002280\">+62 821 1000 2280</a></p>\r\n\r\n<p><a style = \"color:black;\" href=\"icatech.support@itats.ac.id\" target=\"_blank\">icatech.support@itats.ac.id</a></p>\r\n</div>',NULL,0,1),(3,'Contact','                                <p><i class=\"zmdi zmdi-map\"></i> <a style = \"color:black;\" href=\"https://www.google.com/maps/place/Institut+Teknologi+Adhi+Tama+Surabaya/@-7.2900495,112.7790768,15z/data=!4m5!3m4!1s0x0:0x8d971ebe4e20a0d5!8m2!3d-7.2900495!4d112.7790768\" target=\"_blank\">Jl. Arief Rahman Hakim 100, Surabaya</a>\r\n                                <p><i class=\"zmdi zmdi-phone\"></i> <a style = \"color:black;\" href=\"tel:6282110002280\">+62 821 1000 2280</a>\r\n                                <p><i class=\"zmdi zmdi-email\"></i> <a style = \"color:black;\" href=\"icatech.support@itats.ac.id\" target=\"_blank\">icatech.support@itats.ac.id</a>\r\n                                <p><i class=\"zmdi zmdi-globe\"></i> <a style = \"color:black;\" href=\"http://https://icatech.itats.ac.id/2nd/\" target=\"_blank\">ICATECH 2020</a></p>',NULL,0,0);
/*!40000 ALTER TABLE `front_about_countdown` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `front_contact`
--

DROP TABLE IF EXISTS `front_contact`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `front_contact` (
  `front_contact_id` int(11) NOT NULL AUTO_INCREMENT,
  `front_contact_content` text,
  `is_delete` int(11) DEFAULT NULL,
  `is_active` int(11) DEFAULT NULL,
  PRIMARY KEY (`front_contact_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `front_contact`
--

LOCK TABLES `front_contact` WRITE;
/*!40000 ALTER TABLE `front_contact` DISABLE KEYS */;
INSERT INTO `front_contact` VALUES (1,'<h4>More Info :</h4>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Arintha Indah<br />\r\n<a href=\"https://wa.me/6285236476733\" style=\"color: black;\">+62-852-3647-6733</a></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Arintha Indah<br />\r\n<a href=\"https://wa.me/6281398100500\" style=\"color: black;\">+62-813-9810-0500</a></p>\r\n\r\n<p>&nbsp;</p>\r\n',0,1),(2,'<!-- Single Contact Info -->\r\n<div class=\"fadeInUp single-contact-info wow\">\r\n<p>&nbsp;</p>\r\n\r\n<h3>More Info :</h3>\r\n\r\n<p>&nbsp;</p>\r\n</div>\r\n\r\n<div class=\"fadeInUp single-contact-info wow\">\r\n<p>Arintha Indah :</p>\r\n\r\n<h6><a href=\"https://wa.me/6285236476733\" style=\"color: black;\">+62-852-3647-6733</a></h6>\r\n</div>\r\n\r\n<div class=\"fadeInUp single-contact-info wow\">\r\n<p>Nareswarananindya :</p>\r\n\r\n<h6><a href=\"https://wa.me/6281398100500\" style=\"color: black;\">+62-813-9810-0500</a></h6>\r\n</div>\r\n\r\n<div class=\"fadeInUp single-contact-info wow\">\r\n<p>Nareswarananindya :</p>\r\n\r\n<h6><a href=\"https://wa.me/6281398100500\" style=\"color: black;\">+62-813-9810-0500</a></h6>\r\n</div>\r\n\r\n<div>&nbsp;</div>\r\n\r\n<div>\r\n<h5 style=\"margin-left:30px\">Phone<br />\r\n<a href=\"tel:6282110002280\">+62 821 1000 2280</a></h5>\r\n</div>\r\n\r\n<div class=\"media\" style=\"margin-top:30px\">\r\n<div>&nbsp;</div>\r\n\r\n<div>\r\n<h5 style=\"margin-left:28px\">Email<br />\r\n<a href=\"icatech.support@itats.ac.id\" target=\"_blank\">icatech.support@itats.ac.id</a></h5>\r\n</div>\r\n</div>\r\n\r\n<div class=\"media\" style=\"margin-top:30px\">\r\n<div>&nbsp;</div>\r\n\r\n<div>\r\n<h5 style=\"margin-left:30px\">Address<br />\r\n<a href=\"https://www.google.com/maps/place/Institut+Teknologi+Adhi+Tama+Surabaya/@-7.2900495,112.7790768,15z/data=!4m5!3m4!1s0x0:0x8d971ebe4e20a0d5!8m2!3d-7.2900495!4d112.7790768\" target=\"_blank\">Jl. Arief Rahman Hakim 100, Surabaya</a></h5>\r\n</div>\r\n</div>\r\n',0,1),(3,'\r\n                        <div class=\"single-contact-info wow fadeInUp\" data-wow-delay=\"300ms\">\r\n                            <p>Arintha Indah :</p>\r\n                            <h6><a href=\"https://wa.me/6285236476733\" style=\"color: black;\">+62-852-3647-6733</a></h6>\r\n                        </div>\r\n\r\n                       <div class=\"single-contact-info wow fadeInUp\" data-wow-delay=\"300ms\">\r\n                            <p>Nareswarananindya :</p>\r\n                            <h6><a href=\"https://wa.me/6281398100500\" style=\"color: black;\">+62-813-9810-0500</a></h6>\r\n                        </div>\r\n                   ',0,0);
/*!40000 ALTER TABLE `front_contact` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `front_content`
--

DROP TABLE IF EXISTS `front_content`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `front_content` (
  `front_content_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `front_content_title` varchar(255) DEFAULT NULL,
  `front_content_content` text,
  `front_content_add_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `is_active` int(11) DEFAULT NULL,
  `is_delete` int(11) DEFAULT NULL,
  PRIMARY KEY (`front_content_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `front_content`
--

LOCK TABLES `front_content` WRITE;
/*!40000 ALTER TABLE `front_content` DISABLE KEYS */;
INSERT INTO `front_content` VALUES (1,'About The Event','<p>The Second International Conference on Advanced Engineering and Technology (ICATECH) will be hosted by Institut Teknologi Adhi Tama Surabaya (ITATS) on September 26th, 2020. The 2nd ICATECH is an annual event and it is mainly for scientists, researcher','2020-07-26 08:27:29',1,0),(2,'About The Event','<p>The Second International Conference on Advanced Engineering and Technology (ICATECH) will be hosted by Institut Teknologi Adhi Tama Surabaya (ITATS) on September 26th, 2020. The 2nd ICATECH is an annual event and it is mainly for scientists, researchers, and industry with specialties in a related area. The previous conference had been successfully held in Surabaya, September 2018 in coincide with LLDIKTI VII.</p>\r\n\r\n<p>Industry 4.0 has been considered a new industrial stage in which several emerging technologies are converging to provide digital solutions. However, there is a lack of understanding of how companies implement these technologies. Thus, we aim to understand the adoption patterns of Industry 4.0 technologies in manufacturing firms. Sharing efforts to improve safety and sustainability in the construction continue to be developed by all stakeholders ranging from practitioners to academics. Therefore, the main theme for the 2nd ICATECH is Empowering Research and Innovation for Sustainable Technology.</p>\r\n','2020-07-26 08:27:29',0,0);
/*!40000 ALTER TABLE `front_content` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `front_header`
--

DROP TABLE IF EXISTS `front_header`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `front_header` (
  `front_header_id` int(11) NOT NULL AUTO_INCREMENT,
  `front_header_logo` varchar(255) DEFAULT NULL,
  `front_header_nav_key` int(11) DEFAULT NULL,
  `front_header_submit_link` varchar(255) DEFAULT NULL,
  `is_delete` int(11) DEFAULT NULL,
  `is_active` int(11) DEFAULT NULL,
  PRIMARY KEY (`front_header_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `front_header`
--

LOCK TABLES `front_header` WRITE;
/*!40000 ALTER TABLE `front_header` DISABLE KEYS */;
INSERT INTO `front_header` VALUES (1,'FH-d09bf41544a3365a46c9077ebb5e35c3.png',1,'<?php echo base_url(\'front\');?>',0,0);
/*!40000 ALTER TABLE `front_header` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `front_image_slider`
--

DROP TABLE IF EXISTS `front_image_slider`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `front_image_slider` (
  `front_image_slider_id` int(11) NOT NULL AUTO_INCREMENT,
  `front_image_slider_name` varchar(255) DEFAULT NULL,
  `front_image_slider_image` varchar(255) DEFAULT NULL,
  `front_image_slider_name_event` int(10) unsigned DEFAULT NULL,
  `is_delete` int(11) DEFAULT NULL,
  `is_active` int(11) DEFAULT NULL,
  PRIMARY KEY (`front_image_slider_id`),
  KEY `front_image_slider_name_event` (`front_image_slider_name_event`),
  CONSTRAINT `front_image_slider_ibfk_1` FOREIGN KEY (`front_image_slider_name_event`) REFERENCES `periode` (`periode_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `front_image_slider`
--

LOCK TABLES `front_image_slider` WRITE;
/*!40000 ALTER TABLE `front_image_slider` DISABLE KEYS */;
INSERT INTO `front_image_slider` VALUES (1,'1','SI-72b32a1f754ba1c09b3695e0cb6cde7f.png',1,0,0);
/*!40000 ALTER TABLE `front_image_slider` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `front_nav`
--

DROP TABLE IF EXISTS `front_nav`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `front_nav` (
  `front_nav_id` int(11) NOT NULL AUTO_INCREMENT,
  `front_nav_key` int(11) DEFAULT NULL,
  `front_nav_name` varchar(255) DEFAULT NULL,
  `front_nav_link` varchar(255) DEFAULT NULL,
  `is_delete` int(11) DEFAULT NULL,
  `is_active` int(11) DEFAULT NULL,
  `front_nav_child` text,
  `is_parents` int(11) DEFAULT NULL,
  `front_nav_child_link` text,
  PRIMARY KEY (`front_nav_id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `front_nav`
--

LOCK TABLES `front_nav` WRITE;
/*!40000 ALTER TABLE `front_nav` DISABLE KEYS */;
INSERT INTO `front_nav` VALUES (2,1,'Home','front',0,0,NULL,0,NULL),(3,1,'About','https://icatech.itats.ac.id/2nd/about-the-event/',0,0,NULL,0,NULL),(4,1,'Conference Details','#',0,0,'Call,Guideliness,Paper,Committee,Topics,Dates,Fees,Scientifict',0,'https://icatech.itats.ac.id/2nd/conf-details/call-for-papers/,https://icatech.itats.ac.id/2nd/conf-details/author-guidelines/,https://drive.google.com/file/d/1wK5ZuN8wZ9DKpXyMxRr_UzjyxapMfUU-/view,https://icatech.itats.ac.id/2nd/conf-details/committee/,https://icatech.itats.ac.id/2nd/topics/,front/schedule,front/fee,https://icatech.itats.ac.id/2nd/scientific-program/'),(5,1,'Call','https://icatech.itats.ac.id/2nd/conf-details/call-for-papers/',0,0,NULL,1,NULL),(6,1,'Guideliness','https://icatech.itats.ac.id/2nd/conf-details/author-guidelines/',0,0,NULL,1,NULL),(7,1,'Paper','https://drive.google.com/file/d/1wK5ZuN8wZ9DKpXyMxRr_UzjyxapMfUU-/view',0,0,NULL,1,NULL),(8,1,'Committee','https://icatech.itats.ac.id/2nd/conf-details/committee/',0,0,NULL,1,NULL),(9,1,'Topics','https://icatech.itats.ac.id/2nd/topics/',0,0,NULL,1,NULL),(10,1,'Dates','front/schedule',0,0,NULL,1,NULL),(11,1,'Fees','front/fee',0,0,NULL,1,NULL),(12,1,'Scientifict','https://icatech.itats.ac.id/2nd/scientific-program/',0,0,NULL,1,NULL),(13,1,'Past Event','https://icatech.itats.ac.id/1st',0,0,NULL,0,NULL),(14,1,'Contact US','https://icatech.itats.ac.id/2nd/contact-us/',0,0,NULL,0,NULL);
/*!40000 ALTER TABLE `front_nav` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `front_settings`
--

DROP TABLE IF EXISTS `front_settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `front_settings` (
  `front_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `front_title` varchar(255) DEFAULT NULL,
  `front_subtitle` varchar(255) DEFAULT NULL,
  `front_about` varchar(255) DEFAULT NULL,
  `front_color` varchar(255) DEFAULT NULL,
  `is_active` int(11) DEFAULT NULL,
  `is_delete` int(11) DEFAULT NULL,
  PRIMARY KEY (`front_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `front_settings`
--

LOCK TABLES `front_settings` WRITE;
/*!40000 ALTER TABLE `front_settings` DISABLE KEYS */;
INSERT INTO `front_settings` VALUES (1,'ICATECH 2020','ICATECH 2020','','',0,0);
/*!40000 ALTER TABLE `front_settings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `version` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES (3);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `periode`
--

DROP TABLE IF EXISTS `periode`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `periode` (
  `periode_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `periode_name` varchar(255) DEFAULT NULL,
  `periode_theme` varchar(255) DEFAULT NULL,
  `periode_start` date DEFAULT NULL,
  `periode_finish` date DEFAULT NULL,
  `is_active` int(11) DEFAULT NULL,
  `is_delete` int(11) DEFAULT NULL,
  `periode_method` varchar(255) DEFAULT NULL,
  `periode_seats` int(11) DEFAULT NULL,
  `periode_earlybird` date DEFAULT NULL,
  `periode_regular` date DEFAULT NULL,
  PRIMARY KEY (`periode_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `periode`
--

LOCK TABLES `periode` WRITE;
/*!40000 ALTER TABLE `periode` DISABLE KEYS */;
INSERT INTO `periode` VALUES (1,'TEST 1','TEST 1','2020-08-06','2020-08-31',0,0,'Online Confrence',150,'2020-08-12','2020-08-30');
/*!40000 ALTER TABLE `periode` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `price`
--

DROP TABLE IF EXISTS `price`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `price` (
  `id_price` int(11) NOT NULL AUTO_INCREMENT,
  `type_price` int(10) unsigned DEFAULT NULL,
  `total_price` int(11) DEFAULT NULL,
  `period_price` int(10) unsigned DEFAULT NULL,
  `status_price` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_price`),
  KEY `type_price` (`type_price`),
  KEY `period_price` (`period_price`),
  CONSTRAINT `price_ibfk_1` FOREIGN KEY (`type_price`) REFERENCES `type_regis` (`id_type`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `price_ibfk_2` FOREIGN KEY (`period_price`) REFERENCES `periode` (`periode_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `price`
--

LOCK TABLES `price` WRITE;
/*!40000 ALTER TABLE `price` DISABLE KEYS */;
INSERT INTO `price` VALUES (1,1,1850000,1,'EB'),(2,1,1950000,1,'R'),(3,2,1750000,1,'EB'),(4,2,1850000,1,'R'),(5,3,150000,1,'EB'),(6,3,150000,1,'R');
/*!40000 ALTER TABLE `price` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `schedule`
--

DROP TABLE IF EXISTS `schedule`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `schedule` (
  `schedule_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `schedule_name` varchar(255) DEFAULT NULL,
  `schedule_time` date DEFAULT NULL,
  `schedule_period` int(10) unsigned DEFAULT NULL,
  `is_active` int(11) DEFAULT '0',
  `is_delete` int(11) DEFAULT '0',
  PRIMARY KEY (`schedule_id`),
  KEY `schedule_period` (`schedule_period`),
  CONSTRAINT `schedule_ibfk_1` FOREIGN KEY (`schedule_period`) REFERENCES `periode` (`periode_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `schedule`
--

LOCK TABLES `schedule` WRITE;
/*!40000 ALTER TABLE `schedule` DISABLE KEYS */;
INSERT INTO `schedule` VALUES (3,'test2','2020-08-13',1,0,0),(4,'test','2020-08-22',1,0,0);
/*!40000 ALTER TABLE `schedule` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `speakers`
--

DROP TABLE IF EXISTS `speakers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `speakers` (
  `speakers_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `speakers_name` varchar(255) DEFAULT NULL,
  `speakers_job` varchar(255) DEFAULT NULL,
  `speakers_image` varchar(255) DEFAULT NULL,
  `speakers_add_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `is_active` int(11) DEFAULT NULL,
  `is_delete` int(11) DEFAULT NULL,
  `speakers_period` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`speakers_id`),
  KEY `speakers_period` (`speakers_period`),
  CONSTRAINT `speakers_ibfk_1` FOREIGN KEY (`speakers_period`) REFERENCES `periode` (`periode_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `speakers`
--

LOCK TABLES `speakers` WRITE;
/*!40000 ALTER TABLE `speakers` DISABLE KEYS */;
/*!40000 ALTER TABLE `speakers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `submission`
--

DROP TABLE IF EXISTS `submission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `submission` (
  `submission_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `submission_title` varchar(255) DEFAULT NULL,
  `submission_authors` int(10) unsigned DEFAULT NULL,
  `submission_file` varchar(255) DEFAULT NULL,
  `submission_decission` int(11) DEFAULT NULL,
  `submission_upload_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `is_delete` int(11) DEFAULT NULL,
  PRIMARY KEY (`submission_id`),
  KEY `submission_authors` (`submission_authors`),
  CONSTRAINT `submission_ibfk_1` FOREIGN KEY (`submission_authors`) REFERENCES `authors` (`authors_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `submission`
--

LOCK TABLES `submission` WRITE;
/*!40000 ALTER TABLE `submission` DISABLE KEYS */;
INSERT INTO `submission` VALUES (6,'Squidword Tentacles',17,'S-03afdbd66e7929b125f8597834fa83a4.pdf',1,'2020-08-12 10:51:23',0);
/*!40000 ALTER TABLE `submission` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `temp_regis`
--

DROP TABLE IF EXISTS `temp_regis`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `temp_regis` (
  `regis_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `regis_first_name` varchar(255) DEFAULT NULL,
  `regis_last_name` varchar(255) DEFAULT NULL,
  `regis_email` varchar(255) DEFAULT NULL,
  `regis_country` int(10) unsigned DEFAULT NULL,
  `regis_address` varchar(255) DEFAULT NULL,
  `regis_phone_number` varchar(255) DEFAULT NULL,
  `regis_type` int(10) unsigned DEFAULT NULL,
  `regis_period` int(10) unsigned DEFAULT NULL,
  `regis_token` varchar(255) DEFAULT NULL,
  `is_delete` int(11) DEFAULT NULL,
  `is_update` int(11) DEFAULT NULL,
  PRIMARY KEY (`regis_id`),
  KEY `regis_country` (`regis_country`),
  KEY `regis_period` (`regis_period`),
  CONSTRAINT `temp_regis_ibfk_1` FOREIGN KEY (`regis_country`) REFERENCES `country` (`country_id`),
  CONSTRAINT `temp_regis_ibfk_2` FOREIGN KEY (`regis_period`) REFERENCES `periode` (`periode_id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `temp_regis`
--

LOCK TABLES `temp_regis` WRITE;
/*!40000 ALTER TABLE `temp_regis` DISABLE KEYS */;
INSERT INTO `temp_regis` VALUES (1,'Mikazuki','Mikazuki','aidenstacks3@gmail.com',5,'Gedangan','08996444664',1,1,'a2877712c1fd763242a96bdda6f2c684',0,0),(2,'Jacky','Jacky','aidenstacks3@gmail.com',102,'Jagiran Lor','2545454654',3,1,'077ff4b7f1e3e28b76827a6d693959b7',0,1),(3,'Mikazuki','Mikazuki','aidenstacks3@gmail.com',5,'fhfhff','2545454654',3,1,'9ec51c8af8f5976bbf3c469bcb816634',0,1),(4,'Jack','Bauer','aidenstacks3@gmail.com',102,'jagiran','2545454654',3,1,'fc18f6305111b73b83666e088f262807',0,1),(5,'Timmy','Rahadi','aidenstacks3@gmail.com',102,'jojoran','076868595',1,1,'5d80416ab1e617dd7f24161c42421b40',0,1),(6,'Kyon','Kyon','aidenstacks3@gmail.com',2,'jagiran','6354789214',1,1,'ecef1ee01fd5a6da5726930f0ed21d94',0,1),(7,'Mikazuki','Mikazuki','aidenstacks3@gmail.com',102,'LA','0798800',1,1,'d19cb68d91cfefdb5ff688195ea41795',0,1),(8,'Jack','Bauer','aidenstacks3@gmail.com',102,'LA','08996444664',2,1,'ba5147364903e1897a93143a330783d1',0,1),(9,'Jack','Lucky','huracan080899@gmail.com',102,'jojoran','08983331',2,1,'ca01dda4cd79c370fc93d8937a9ebfc0',0,1),(10,'Mikazuki','Mikazuki','aidenstacks3@gmail.com',5,'Jagiran Lor','08996444664',1,1,'c6d60e8575d0ed72363d9fbd7e4a7bc4',0,0),(11,'Jacky ','Lucky','aidenstacks3@gmail.com',102,'Gedangan Kulon Wetan','6354789214',2,1,'2768388ff0e7fb362943c5a4490d112b',0,1);
/*!40000 ALTER TABLE `temp_regis` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `transaction_regis`
--

DROP TABLE IF EXISTS `transaction_regis`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `transaction_regis` (
  `transaction_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `transaction_autors` int(10) unsigned DEFAULT NULL,
  `transaction_type` int(10) unsigned DEFAULT NULL,
  `transaction_additional` text,
  `transaction_total` int(11) DEFAULT NULL,
  `is_delete` int(11) DEFAULT NULL,
  `is_pay` int(11) DEFAULT NULL,
  `transaction_pay_date` date DEFAULT NULL,
  PRIMARY KEY (`transaction_id`),
  KEY `transaction_autors` (`transaction_autors`),
  CONSTRAINT `transaction_regis_ibfk_1` FOREIGN KEY (`transaction_autors`) REFERENCES `authors` (`authors_id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `transaction_regis`
--

LOCK TABLES `transaction_regis` WRITE;
/*!40000 ALTER TABLE `transaction_regis` DISABLE KEYS */;
INSERT INTO `transaction_regis` VALUES (8,NULL,3,NULL,3420000,0,0,NULL),(13,17,2,'0',1820000,0,0,NULL);
/*!40000 ALTER TABLE `transaction_regis` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `type_regis`
--

DROP TABLE IF EXISTS `type_regis`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `type_regis` (
  `id_type` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name_type` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_type`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `type_regis`
--

LOCK TABLES `type_regis` WRITE;
/*!40000 ALTER TABLE `type_regis` DISABLE KEYS */;
INSERT INTO `type_regis` VALUES (1,'Authors'),(2,'Authors (College)'),(3,'Participant');
/*!40000 ALTER TABLE `type_regis` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `users_id` int(5) unsigned NOT NULL AUTO_INCREMENT,
  `users_name` varchar(100) NOT NULL,
  `users_password` varchar(100) NOT NULL,
  `last_login` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `is_login` int(2) NOT NULL,
  `is_active` int(11) DEFAULT NULL,
  `url_token` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`users_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'aidenstacks3@gmail.com','5536','2020-08-09 23:57:48',0,0,'a2877712c1fd763242a96bdda6f2c684'),(2,'aidenstacks3@gmail.com','0289','2020-08-11 15:38:55',0,0,'5d80416ab1e617dd7f24161c42421b40'),(3,'aidenstacks3@gmail.com','3137','2020-08-12 04:34:26',0,0,'ecef1ee01fd5a6da5726930f0ed21d94'),(4,'aidenstacks3@gmail.com','6074','2020-08-12 04:39:29',0,0,'d19cb68d91cfefdb5ff688195ea41795'),(5,'aidenstacks3@gmail.com','0457','2020-08-12 04:39:58',0,0,'ba5147364903e1897a93143a330783d1'),(6,'huracan080899@gmail.com','1901','2020-08-12 05:18:22',0,0,'ca01dda4cd79c370fc93d8937a9ebfc0'),(7,'aidenstacks3@gmail.com','7861','2020-08-12 10:29:11',0,0,'c6d60e8575d0ed72363d9fbd7e4a7bc4'),(8,'aidenstacks3@gmail.com','1498','2020-08-12 10:31:28',0,0,'2768388ff0e7fb362943c5a4490d112b');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-08-12 22:26:10
