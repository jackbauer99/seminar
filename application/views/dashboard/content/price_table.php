<?php

defined('BASEPATH') OR exit('No direct script access allowed');

?>

<?php

defined('BASEPATH') OR exit('No direct script access allowed');

?>

		<!-- Begin Page Content -->
        <div class="container-fluid">

          <!-- Page Heading -->
           <div class="d-sm-flex align-items-center justify-content-between mb-4">
          <h1 class="h3 mb-2 text-gray-800"><?php echo $subtitle;?></h1>
          <a href="#myModal" data-toggle="modal" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="fas fa-plus fa-sm text-white-50"></i> Add <?php echo $subtitle;?></a>
      </div>

		<!-- DataTales Example -->
          <div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary"><?php echo $subtitle;?></h6>
            </div>
            <div class="card-body">
            	<!-- Alert -->
          <?php if($this->session->flashdata('success') || $this->session->flashdata('failed')) {
            if($this->session->flashdata('success')) {
              $alert = 'success';
              $status = $alert;
            } else if($this->session->flashdata('failed')) {
              $alert = 'danger';
              $status = 'failed';
            }
            ?>
            <div class="alert alert-<?php echo $alert;?> alert-dismissible fade show" role="alert">
              <?php echo $this->session->flashdata($status);?>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
          <?php } ?>
              <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <thead>
                  	<tr>
                    <?php foreach($table as $key => $value) {
                    	?>
                    		<th><?php echo $value;?></th>
                  	<?php } ?>
                  	</tr>
                  </thead>
                  <tfoot>
                    <tr>
                    <?php foreach($table as $key => $value) {
                    	?>
                    		<th><?php echo $value;?></th>
                  	<?php } ?>
                  	</tr>
                  </tfoot>
                  <tbody>
                  	<?php $no = 1; foreach($row as $rows => $val) {
                  		?>
                  		<tr>
                  			<td><?php echo $no++;?></td>
                        <td><?php echo $rows;?></td>
                        <td><?php echo $val['EB'];?></td>
                        <td><?php echo $val['R'];?></td>
                  		<!-- <?php foreach($column as $key => $value) {
                  			?>
                  				<td><?php echo $rows[$value];?></td>
                  	<?php	} ?> -->
                  	<!-- <td><a href="#myModal<?php echo $rows[$id];?>" data-toggle="modal" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="fas fa-eye fa-sm text-white-50"></i></a></td> -->
                  </tr>
                 <?php 	}  ?>
                  </tbody>
                  </table>
              </div>
            </div>
          </div>

        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->
