<?php

defined('BASEPATH') OR exit('No direct script access allowed');

?>

<?php foreach($detailrow as $rows) {
  ?>
  <div id="myModal<?php echo $rows[$id];?>" class="modal fade">
       <div class="col-sm-12 col-12">
           <div class="modal-dialog modal-lg">
               <div class="modal-content">
                   <div class="modal-header">
                       <h4 class="modal-title"><?php echo $rows[$detailname];?></h4>
                       <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                   </div>
                   <div class="modal-body">
               <div class="row">
                 <div class="col-lg-12">
                    <table class="table table-bordered">
                        <?php foreach($detailcolumn as $key => $value) {
                          ?>
                          <tr>
                              <td><?php echo "<b>".$detailtable[$key]."</b> : ".$rows[$value];?></td>
                            </tr>
                      <?php  } ?>
                    </table>
                  </div>
         
          </div>
        </div>
      </div>
    </div>
  </div>
</div>   
   <!-- End Modal HTML -->
<?php } ?>
