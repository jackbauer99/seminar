<?php

defined('BASEPATH') OR exit('No direct script access allowed');

?>

<?php foreach($row as $rows) : ?>
<div id="editable<?php echo $rows[$id];?>" class="modal fade">
       <div class="col-sm-12 col-12">
           <div class="modal-dialog modal-login">
               <div class="modal-content">
                   <div class="modal-header">
                       <h4 class="modal-title"><?php echo $subtitle;?></h4>
                       <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                   </div>
                   <div class="modal-body">
                       <form action="<?php echo base_url($url);?>" method="post" enctype="multipart/form-data">
          <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>"
            value="<?php echo $this->security->get_csrf_hash(); ?>">
          <?php foreach($form as $forms) {
            ?>
            <?php if($forms['type'] == 'text' || $forms['type'] == 'email' || $forms['type'] == 'password' || $forms['type'] == 'number') {
              ?>
              <div class="form-group">
                <label><?php echo $forms['label'];?></label>
                  <input type="<?php echo $forms['type'];?>" class="form-control" name="<?php echo $forms['name'];?>" placeholder="<?php echo $forms['placeholder'];?>" required="<?php echo $forms['required'];?>">
              </div>
            <?php } else if($forms['type'] == 'select') {
              ?>
              <div class="form-group">
                <label><?php echo $forms['label'];?></label>
                <select class="" style="width: 100%" id="<?php echo $forms['id'];?>" name="<?php echo $forms['name'];?>">
                  <?php if($forms['op'] == 'F') {
                  foreach($forms['value'] as $key => $value) {
                      ?>
                      <option value="<?php echo $key;?>"><?php echo $value;?></option>
                  <?php }
                } else if($forms['op'] == 'N') {
                  ?>
                    <option value="NULL">--- Select <?php echo $forms['label'];?> ---</option>
                  <?php foreach($forms['value'] as $key => $value) {
                    ?>
                    <option value="<?php echo $key;?>"><?php echo $value;?></option>
                <?php }
                } ?>
              </select>
              </div>
            <?php } else if($forms['type'] == 'textarea') {
              ?>
              <div class="form-group">
                <label><?php echo $forms['label'];?></label>
                  <textarea name="<?php echo $forms['name'];?>" id="<?php echo $forms['id'];?>" placeholder="<?php echo $forms['placeholder'];?>"></textarea>
              </div>
          <?php  } else if($forms['type'] == 'date') {
            ?>
            <div class="form-group">
              <label for="<?php echo $forms['label'];?>"><?php echo $forms['label'];?></label>
              <input type="<?php echo $forms['type'];?>" name="<?php echo $forms['name'];?>" class="form-control datepicker" id="<?php echo $forms['id'];?>" placeholder="<?php echo $forms['placeholder'];?>">
            </div>
          <?php  } else if($forms['type'] == 'file') {
            ?>
              <div class="form-group">
                <div class="custom-file">
                  <input type="<?php echo $forms['type'];?>" class="custom-file-input" id="customFile"  name="<?php echo $forms['name'];?>" onchange="renderImg(this)">
                  <label class="custom-file-label" for="customFile"><?php echo $forms['label'];?></label>
                </div>
              </div>
          <?php  } else if($forms['type'] == 'color') {
            ?>
          <div class="form-group">
            <label for="<?php echo $forms['label'];?>"><?php echo $forms['label'];?></label>
            <input type="text" name="<?php echo $forms['name'];?>" class="form-control datepicker" id="<?php echo $forms['id'];?>" placeholder="<?php echo $forms['placeholder'];?>">
          </div>
          <?php }
          } ?>
          <div class="form-group">
            <button type="submit" class="btn btn-primary">Save</button>
            <button type="reset" class="btn btn-warning">Reset</button>
          </div>
        </form>
           </div>
       </div>
   </div>
</div>
</div>
<?php endforeach ; ?>
   <!-- End Modal HTML -->
