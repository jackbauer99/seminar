<?php

defined('BASEPATH') OR exit('No direct script access allowed');

?>
<!-- Begin Page Content -->
<div class="container-fluid">

  <!-- Page Heading -->
  <div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800"><?php echo $subtitle;?></h1>
  </div>

  <!-- Content Row -->
  <div class="row">

    <!-- Content Column -->
    <div class="col-lg-12">

      <!-- Project Card Example -->
      <div class="card shadow mb-12">
        <div class="card-header py-6">
          <h6 class="m-0 font-weight-bold text-primary"><?php echo $subtitle;?></h6>
        </div>
        <div class="card-body">
          <!-- Alert -->
          <?php if($this->session->flashdata('success') || $this->session->flashdata('failed')) {
            if($this->session->flashdata('success')) {
              $alert = 'success';
              $status = $alert;
            } else if($this->session->flashdata('failed')) {
              $alert = 'danger';
              $status = 'failed';
            }
            ?>
            <div class="alert alert-<?php echo $alert;?> alert-dismissible fade show" role="alert">
              <?php echo $this->session->flashdata($status);?>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
          <?php } ?>
          <form action="<?php echo base_url($url);?>" method="post" enctype="multipart/form-data">
          <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>"
            value="<?php echo $this->security->get_csrf_hash(); ?>">
          <?php foreach($form as $forms) {
            ?>
            <?php if($forms['type'] == 'text' || $forms['type'] == 'email' || $forms['type'] == 'password') {
              ?>
              <div class="form-group">
                <label><?php echo $forms['label'];?></label>
                  <input type="<?php echo $forms['type'];?>" class="form-control" name="<?php echo $forms['name'];?>" placeholder="<?php echo $forms['placeholder'];?>" required="<?php echo $forms['required'];?>"
                  value = "<?php echo (!empty($forms['value'])) ? $forms['value'] : null ;?>">
              </div>
            <?php } else if($forms['type'] == 'select') {
              ?>
              <div class="form-group">
                <label><?php echo $forms['label'];?></label>
                <select class="" style="width: 100%" id="<?php echo $forms['id'];?>" name="<?php echo $forms['name'];?>">
                  <?php if($forms['op'] == 'F') {
                  foreach($forms['value'] as $key => $value) {
                      ?>
                      <option value="<?php echo $key;?>"><?php echo $value;?></option>
                  <?php }
                } else if($forms['op'] == 'N') {
                  ?>
                    <option value="NULL">--- Select <?php echo $forms['label'];?> ---</option>
                  <?php foreach($forms['value'] as $key => $value) {
                    ?>
                    <option value="<?php echo $key;?>"><?php echo $value;?></option>
                <?php }
                } ?>
              </select>
              </div>
            <?php } else if($forms['type'] == 'textarea') {
              ?>
              <div class="form-group">
                <label><?php echo $forms['label'];?></label>
                  <textarea name="<?php echo $forms['name'];?>" id="<?php echo $forms['id'];?>" placeholder="<?php echo $forms['placeholder'];?>"></textarea>
              </div>
          <?php  } else if($forms['type'] == 'date') {
            ?>
            <div class="form-group">
              <label for="<?php echo $forms['label'];?>"><?php echo $forms['label'];?></label>
              <input type="<?php echo $forms['type'];?>" name="<?php echo $forms['name'];?>" class="form-control datepicker" id="<?php echo $forms['id'];?>" placeholder="<?php echo $forms['placeholder'];?>" value = "<?php echo (!empty($forms['value'])) ? $forms['value'] : null ;?>">
            </div>
          <?php  } else if($forms['type'] == 'file') {
            ?>
              <div class="form-group">
                <div class="custom-file">
                  <input type="<?php echo $forms['type'];?>" class="custom-file-input" id="customFile"  name="<?php echo $forms['name'];?>" onchange="renderImg(this)">
                  <label class="custom-file-label" for="customFile"><?php echo $forms['label'];?></label>
                </div>
              </div>
          <?php  } else if($forms['type'] == 'color') {
            ?>
          <div class="form-group">
            <label for="<?php echo $forms['label'];?>"><?php echo $forms['label'];?></label>
            <input type="text" name="<?php echo $forms['name'];?>" class="form-control datepicker" id="<?php echo $forms['id'];?>" placeholder="<?php echo $forms['placeholder'];?>">
          </div>
          <?php }
          } ?>
          <div class="form-group">
            <button type="submit" class="btn btn-primary">Save</button>
            <button type="reset" class="btn btn-warning">Reset</button>
          </div>
        </form>
        </div>
      </div>
    </div>

</div>
<!-- /.container-fluid -->

</div>
<!-- End of Main Content -->
