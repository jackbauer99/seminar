<?php

defined('BASEPATH') OR exit('No direct script access allowed');

?>

<!-- Bootstrap core JavaScript-->
<script src="<?php echo base_url();?>assets/dashboard/vendor/jquery/jquery.min.js"></script>
<script src="<?php echo base_url();?>assets/dashboard/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

<!-- Core plugin JavaScript-->
<script src="<?php echo base_url();?>assets/dashboard/vendor/jquery-easing/jquery.easing.min.js"></script>

<!-- Custom scripts for all pages-->
<script src="<?php echo base_url();?>assets/dashboard/js/sb-admin-2.min.js"></script>

<!-- Page level plugins -->
<script src="<?php echo base_url();?>assets/dashboard/vendor/datatables/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url();?>assets/dashboard/vendor/datatables/dataTables.bootstrap4.min.js"></script>

<!-- Page level custom scripts -->
<script src="<?php echo base_url();?>assets/dashboard/js/demo/datatables-demo.js"></script>

<!-- Page level plugins -->
<script src="<?php echo base_url();?>assets/dashboard/vendor/chart.js/Chart.min.js"></script>

<!-- Page level custom scripts -->
<script src="<?php echo base_url();?>assets/dashboard/js/demo/chart-area-demo.js"></script>
<script src="<?php echo base_url();?>assets/dashboard/js/demo/chart-pie-demo.js"></script>

<!-- Datepicker -->
<script src="<?php echo base_url();?>assets/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>

<!-- CK Editor -->
<script src="<?php echo base_url();?>assets/ckeditor/ckeditor.js"></script>

<!-- Select2 -->
<script src="<?php echo base_url();?>assets/select2/js/select2.min.js"></script>

<!-- ColorPicker -->
<script src="<?php echo base_url();?>assets/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js"></script>

<!-- BS File Input -->
<script src="<?php echo base_url();?>assets/bs-custom-file-input/bs-custom-file-input.min.js"></script>


<script>
$(document).ready(function() {
  <?php foreach($form as $forms) {
    if($forms['type'] == 'date') {
      ?>
      $('#<?php echo $forms['id'];?>').datepicker({
        format: 'yyyy-mm-dd',
      });
    <?php } else if($forms['type'] == 'textarea') {
      ?>
      CKEDITOR.replace('<?php echo $forms['id'];?>');
  <?php } else if($forms['type'] == 'select') {
    ?>
    $('#<?php echo $forms['id'];?>').select2({
      theme: 'bootstrap4',
    });
  <?php } else if($forms['type'] == 'color') {
    ?>
    $('#<?php echo $forms['id'];?>').colorpicker();
<?php }
  } ?>

  $('#editable').select2({
    theme: 'bootstrap4',
  });

  bsCustomFileInput.init();



});
</script>

</body>

</html>
