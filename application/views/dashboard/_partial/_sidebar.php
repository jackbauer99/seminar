<?php

defined('BASEPATH') OR exit('No direct script access allowed');

?>

<body id="page-top">

  <!-- Page Wrapper -->
  <div id="wrapper">

    <!-- Sidebar -->
    <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

      <!-- Sidebar - Brand -->
      <a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.html">
        <?php foreach($base as $settings) {
          ?>
          <div class="sidebar-brand-text mx-3"><?php echo $settings['base_company_name'];?></div>
      <?php } ?>
      </a>

      <!-- Divider -->
      <hr class="sidebar-divider my-0">

      <!-- Nav Item - Dashboard -->
      <li class="nav-item <?php if(isset($dashboard)) {
        echo $dashboard;
      }; ?>">
        <a class="nav-link" href="<?php echo base_url('back/');?>">
          <i class="fas fa-fw fa-tachometer-alt"></i>
          <span>Dashboard</span></a>
      </li>

      <!-- Nav Item - Dashboard -->

      <!-- Divider -->
      <hr class="sidebar-divider">

      <!-- Heading -->
      <div class="sidebar-heading">
        Interface
      </div>

      <!-- Nav Item - Pages Collapse Menu -->
      <li class="nav-item <?php if(isset($basesettings)) {
        echo $basesettings;
      }; ?>">
        <a class="nav-link" href="<?php echo base_url('back/basesettings');?>">
          <i class="fas fa-fw fa-cog"></i>
          <span>Base Settings</span></a>
      </li>
      <li class="nav-item <?php if(isset($frontsettings)){
        echo $frontsettings;
      }; ?>">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
          <i class="fas fa-fw fa-cog"></i>
          <span>Front Settings</span>
        </a>
        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <h6 class="collapse-header">Front Settings:</h6>
            <a class="collapse-item" href="<?php echo base_url('back/frontsettings');?>">Front Settings</a>
            <a class="collapse-item" href="<?php echo base_url('back/frontheader');?>">Front Header</a>
            <a class="collapse-item" href="<?php echo base_url('back/frontnav');?>">Front Nav</a>
            <a class="collapse-item" href="<?php echo base_url('back/frontimageslider');?>">Front Image Slider</a>
            <a class="collapse-item" href="<?php echo base_url('back/frontcontent');?>">Front Content</a>
            <a class="collapse-item" href="<?php echo base_url('back/frontcontact');?>">Front Contact</a>
            <a class="collapse-item" href="<?php echo base_url('back/frontabout');?>">Front About</a>
          </div>
        </div>
      </li>


      <!-- Divider -->
      <hr class="sidebar-divider d-none d-md-block">

      <!-- Heading -->
      <div class="sidebar-heading">
        Event Management
      </div>

      <!-- Nav Item - Pages Collapse Menu -->
      <li class="nav-item <?php if(isset($eventsettings)){
        echo $eventsettings;
      }; ?>">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseThree" aria-expanded="true" aria-controls="collapseTwo">
          <i class="fas fa-fw fa-cog"></i>
          <span>Event Settings</span>
        </a>
        <div id="collapseThree" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <h6 class="collapse-header">Event Settings:</h6>
            <a class="collapse-item" href="<?php echo base_url('back/periodsettings');?>">Period Settings</a>
            <a class="collapse-item" href="<?php echo base_url('back/speakers');?>">Speakers</a>
            <a class="collapse-item" href="<?php echo base_url('back/schedule');?>">Schedule</a>
          </div>
        </div>
      </li>

      <!-- Divider -->
      <hr class="sidebar-divider d-none d-md-block">

      <!-- Heading -->
      <div class="sidebar-heading">
        Master Management
      </div>

      <!-- Nav Item - Pages Collapse Menu -->
      <li class="nav-item <?php if(isset($master)){
        echo $master;
      }; ?>">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseFour" aria-expanded="true" aria-controls="collapseFour">
          <i class="fas fa-fw fa-cog"></i>
          <span>Master</span>
        </a>
        <div id="collapseFour" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <h6 class="collapse-header">Master :</h6>
            <a class="collapse-item" href="<?php echo base_url('back/authorstype');?>">Regis Type</a>
            <a class="collapse-item" href="<?php echo base_url('back/pricetype');?>">Price Type</a>
            <a class="collapse-item" href="<?php echo base_url('back/authors');?>">Authors</a>
            <a class="collapse-item" href="<?php echo base_url('back/submission');?>">Authors Submission</a>
          </div>
        </div>
      </li>

      <!-- Sidebar Toggler (Sidebar) -->
      <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
      </div>

    </ul>
    <!-- End of Sidebar -->
