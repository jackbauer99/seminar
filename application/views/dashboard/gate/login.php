<?php

defined('BASEPATH') OR exit('No direct script access allowed');

?>

<body class="bg-gradient-primary">

  <div class="container">

    <!-- Outer Row -->
    <div class="row justify-content-center">

      <div class="col-xl-5 col-lg-12 col-md-12">

          <div class="card-body p-0">
            <div class="card o-hidden border-0 shadow-lg my-5">
            <!-- Nested Row within Card Body -->
            <div class="row">
              <!-- <div class="col-lg-6 d-none d-lg-block bg-login-image"></div> -->
              <div class="col-lg-12">
                <div class="p-5">

                  <!-- Menampilkan pesan error diambil dari validate -->
                  <?php if($this->session->flashdata('failed')) {
                    ?>
                    <div class="alert alert-danger">
                      <ul class="mb-0">
                      <?php echo $this->session->flashdata('failed');?>
                    </ul>
                    </div>
                  <?php } ?>

                  <div class="text-center">
                    <h1 class="h4 text-gray-900 mb-4"><?php echo $subtitle;?></h1>
                  </div>
                  <form class="user" method="post" action="<?php echo base_url('gate/auth');?>">
                    <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>"
                    value="<?php echo $this->security->get_csrf_hash(); ?>">
                    <?php foreach($form as $forms) {
                      ?>
                      <?php if($forms['type'] == 'text' || $forms['type'] == 'password' || $forms['type'] == 'email') {
                        ?>
                        <div class="form-group">
                          <input type="<?php echo $forms['type'];?>" class="form-control form-control-user" id="<?php echo $forms['id'];?>" name = "<?php echo $forms['name'];?>" aria-describedby="emailHelp" placeholder="<?php echo $forms['placeholder'];?>">
                        </div>
                      <?php } else if($forms['type'] == 'checkbox') {
                        ?>
                        <div class="form-group">
                          <div class="custom-control custom-checkbox small">
                            <input type="<?php $forms['type'];?>" name="<?php echo $forms['name'];?>" class="custom-control-input" id="customCheck">
                            <label class="custom-control-label" for="customCheck"><?php echo $forms['label'];?></label>
                          </div>
                        </div>
                      <?php } ?>
                    <?php } ?>

                    <button type="submit" class="btn btn-primary btn-user btn-block">
                      <?php echo $button;?>
                    </button>
                    <!-- <hr>
                    <a href="<?php echo base_url('gate/eng');?>" class="btn btn-google btn-user btn-block">
                      <?php echo $languange.$ENG;?>
                    </a>
                    <a href="<?php echo base_url('gate/idn');?>" class="btn btn-google btn-user btn-block">
                      <?php echo $languange.$IDN;?>
                    </a> -->
                  </form>
                  <hr>
                  <?php if($forgot_password['active']) {
                    ?>
                    <div class="text-center">
                      <a class="small" href="forgot-password.html"><?php echo $forgot_password['text'];?></a>
                    </div>
                <?php  } ?>
                </div>
              </div>
            </div>
          </div>
        </div>

      </div>

    </div>

  </div>
