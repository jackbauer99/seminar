<?php

defined('BASEPATH') OR exit('No direct script access allowed');

?>

<!-- **** All JS Files ***** -->
<!-- jQuery 2.2.4 -->
<script src="<?php echo base_url();?>assets/landing_pagev2/js/jquery.min.js"></script>
<!-- Popper -->
<script src="<?php echo base_url();?>assets/landing_pagev2/js/popper.min.js"></script>
<!-- Bootstrap -->
<script src="<?php echo base_url();?>assets/landing_pagev2/js/bootstrap.min.js"></script>
<!-- All Plugins -->
<script src="<?php echo base_url();?>assets/landing_pagev2/js/confer.bundle.js"></script>
<!-- Active -->
<script src="<?php echo base_url();?>assets/landing_pagev2/js/default-assets/active.js"></script>
<!-- Select2 -->
<script src="<?php echo base_url();?>assets/select2/js/select2.min.js"></script>
<!-- Active -->
<script src="<?php echo base_url();?>assets/landing_pagev2/js/counter.js"></script>

<script>

$(document).ready(function() {
  <?php foreach($form as $forms) {
    if($forms['type'] == 'select') {
      ?>
      $('#<?php echo $forms['id'];?>').select2({
        theme: 'bootstrap4'
        <?php if(!empty($forms['multiple'])) {
          ?>,
          placeholder: "<?php echo $forms['placeholder'];?>",
          allowClear: true
       <?php } ?>
      });
    <?php }

  } ?>

  if ($.fn.countdown) {
  	<?php foreach($period as $periods) {
  		?>
  		$("#clock").countdown("<?php echo $periods['periode_finish'];?>", function (event) {
            $(this).html(event.strftime("<div>%m <span>Months</span></div> <div>%d <span>Days</span></div> <div>%H <span>Hours</span></div> <div>%M <span>Minutes</span></div> <div>%S <span>Seconds</span></div>"));
        });
  	<?php } ?>
        
    }
});

</script>

</body>

</html>
