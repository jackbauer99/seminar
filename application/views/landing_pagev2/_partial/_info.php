<?php

defined('BASEPATH') OR exit('No direct script access allowed');

?>

<section style="padding-bottom: 40px; background-color: #f6f6f6;
background-image: linear-gradient(315deg, #f6f6f6 0%, #e9e9e9 74%);">
        <div class="container">
            <div class="row">
            <br/>
                <div style="margin-top: 40px; margin-bottom: 20px;" class="col text-center">
                    <h2 style="color: black;">Event Information</h2>
                    <p style="color: white;"></p>
                </div>
            </div>
            <div class="row text-center">
                <div class="col-6 col-sm-3">
                    <div class="couner">
                        <center>
                            <i style="color: white; text-align: center;"  class="fa fa-microphone fa-2x"></i>
                        </center>
                        <h2 class="timer count-title count-number" data-to="<?php echo $speakerscount['total'];?>" data-speed="1500"></h2>
                        <p class="count-text ">Speaker (Confirmed)</p>
                    </div>
                </div>
                <div class="col-6 col-sm-3">
                    <div class="couner">
                        <center>
                            <i style="color: white;" class="fa fa-flag fa-2x"></i>
                        </center>
                        <h2 class="timer count-title count-number" data-to="1" data-speed="1500"></h2>
                        <p class="count-text ">Sponsor (Confirmed)</p>
                    </div>
                </div>
                <div class="col-6 col-sm-3">
                    <div class="couner">
                        <center>
                            <i style="color: white;" class="fa fa-wheelchair fa-2x"></i>
                        </center>
                        <?php foreach($period as $periods) {
                            ?>
                            <h2 class="timer count-title count-number" data-to="<?php echo $periods['periode_seats'] - $authors['total'];?>" data-speed="1500"></h2>
                     <?php  } ?>
                        <p class="count-text ">Seats</p>
                </div></div>
                <div class="col-6 col-sm-3">
                    <div class="couner">
                        <center>
                            <i style="color: white;" class="fa fa-user fa-2x"></i>
                        </center>
                        <h2 class="timer count-title count-number" data-to="<?php echo $authors['total'];?>" data-speed="1500"></h2>
                        <p class="count-text ">Registrant</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
