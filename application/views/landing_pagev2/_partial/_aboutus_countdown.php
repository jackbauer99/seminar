<?php

defined('BASEPATH') OR exit('No direct script access allowed');

?>

<!-- About Us And Countdown Area Start -->
<section class="about-us-countdown-area section-padding-100-0" id="about">
    <div class="container">
        <div class="row align-items-center">
            <!-- About Content -->
            <div class="col-12 col-md-6">
                <div class="about-content-text mb-80">
                    <?php foreach($content as $contents) {
                        ?>
                         <h3 class="wow fadeInUp" data-wow-delay="300ms"><?php echo $contents['front_content_title'];?></h3>
                            <p class="wow fadeInUp" data-wow-delay="300ms"><?php echo $contents['front_content_content'];?>
                                <a href="#" class="btn confer-btn mt-50 wow fadeInUp" data-wow-delay="300ms">Interested <i class="zmdi zmdi-long-arrow-right"></i></a>
                <?php  }  ?>
                </div>
            </div>

            <!-- About Thumb -->
            <div class="col-12 col-md-6">
                <div class="about-thumb mb-80 wow fadeInUp" data-wow-delay="300ms">
                    <img src="<?php echo base_url();?>assets/image/icatech2018-all-participants-01.jpg" alt="">
                </div>
            </div>
        </div>
    </div>

    <!-- Counter Up Area -->
    <div class="countdown-up-area">
        <div class="container">
            <div class="row align-items-center">

                <div class="col-12 col-md-12">
                    <div class="countdown-timer mb-100 wow fadeInUp" data-wow-delay="300ms">
                        <div id="clock"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- About Us And Countdown Area End -->
