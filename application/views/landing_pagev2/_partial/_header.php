<?php

defined('BASEPATH') OR exit('No direct script access allowed');

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="description" content="">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Title -->
    <?php foreach($base as $fronts) {
        ?>
        <title><?php echo $fronts['front_title'];?></title>
    <?php } ?>

    <!-- Favicon -->
    <link rel="icon" href="./img/core-img/favicon.png">

    <!-- Stylesheet -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/landing_pagev2/style.css">
    <!-- Stylesheet -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/landing_pagev2/css/counter.css">

    <!-- Select2 -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/select2/css/select2-bootstrap4.min.css">
    <link rel="stylesheet" href="<?php echo base_url();?>assets/select2/css/select2.min.css">


</head>

<!-- Menampilkan Notifikasi -->
<?php if($this->session->flashdata('success')) {
  echo $this->session->flashdata('success');
} else if($this->session->flashdata('failed')) {
  echo $this->session->flashdata('failed');
} ?>

<body>
    <!-- Preloader -->
    <div id="preloader">
        <div class="loader"></div>
    </div>
    <!-- /Preloader -->

    <!-- Header Area Start -->
    <header class="header-area">
        <div class="classy-nav-container breakpoint-off">
            <div class="container">
                <!-- Classy Menu -->
                <nav class="classy-navbar justify-content-between" id="conferNav">

                    <!-- Logo -->
                    <?php foreach($header as $headers) {
                        ?>
                        <a class="nav-brand" href="<?php echo base_url();?>"><img src="<?php echo base_url();?>assets/image/Front/<?php echo $headers['front_header_logo'];?>" alt="" width="50%"></a>
                    <?php } ?>

                    <!-- Navbar Toggler -->
                    <div class="classy-navbar-toggler">
                        <span class="navbarToggler"><span></span><span></span><span></span></span>
                    </div>

                    <!-- Menu -->
                    <div class="classy-menu">
                        <!-- Menu Close Button -->
                        <div class="classycloseIcon">
                            <div class="cross-wrap"><span class="top"></span><span class="bottom"></span></div>
                        </div>
                        <!-- Nav Start -->
                        <div class="classynav">
                            <ul id="nav">
                                <?php foreach($nav as $navs){
                                    if($navs['front_nav_child'] != NULL) {
                                        ?>
                                        <li><a href="#"><?php echo $navs['front_nav_name'];?></a>
                                            <ul class="dropdown">
                                                <?php foreach($navs['front_nav_child'] as $key => $value) {
                                                    // foreach($navs['front_nav_child_link'] as $key2 => $value2) {
                                                        ?>
                                                        <li><a href="<?php echo $navs['front_nav_child_link'][$key];?>"><?php echo $value;?></a></li>
                                                <?php    //}
                                                } ?>
                                        </li>
                                    </ul>
                                 <?php   } else {
                                    ?>
                                    <li><a href="<?php echo $navs['front_nav_link'];?>"><?php echo $navs['front_nav_name'];?></a></li>
                              <?php   }
                                }  ?>
                                <!-- <li><a href="#">Pages</a>
                                    <ul class="dropdown">
                                        <li><a href="index.html">- Home</a></li>
                                        <li><a href="about.html">- About Us</a></li>
                                        <li><a href="speakers.html">- Speakears</a></li>
                                        <li><a href="schedule.html">- Schedule</a></li>
                                        <li><a href="tickets.html">- Tickets</a></li>
                                        <li><a href="blog.html">- Blog</a></li>
                                        <li><a href="single-blog.html">- Single Blog</a></li>
                                        <li><a href="contact.html">- Contact</a></li>
                                        <li><a href="#">- Dropdown</a>
                                            <ul class="dropdown">
                                                <li><a href="#">- Dropdown Item</a></li>
                                                <li><a href="#">- Dropdown Item</a></li>
                                                <li><a href="#">- Dropdown Item</a></li>
                                                <li><a href="#">- Dropdown Item</a></li>
                                            </ul>
                                        </li>
                                    </ul>
                                </li>
                                <li><a href="speakers.html">Speakears</a></li>
                                <li><a href="blog.html">Blog</a></li>
                                <li><a href="contact.html">Contact</a></li> -->
                                        </ul>

                            <!-- Get Tickets Button -->
                            <?php foreach($header as $headers) {
                                ?>
                                <a href="#login" data-toggle="modal" class="btn trigger-btn confer-btn m-2">SUBMIT PAPER <i class="zmdi zmdi-long-arrow-right"></i></a>
                            <?php } ?>
                        </div>
                        <!-- Nav End -->
                    </div>
                </nav>
            </div>
        </div>
    </header>
    <!-- Header Area End -->
