<?php

defined('BASEPATH') OR exit('No direct script access allowed');

?>

<div id="login" class="modal fade">
       <div class="col-sm-12 col-12">
           <div class="modal-dialog modal-login">
               <div class="modal-content">
                   <div class="modal-header">
                       <h4 class="modal-title">Conference Login</h4>
                       <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                   </div>
                   <div class="modal-body">
                       <form method="post" action="<?php echo base_url('front/loginact');?>">
                         <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>"
                         value="<?php echo $this->security->get_csrf_hash(); ?>">
                         <?php foreach($login as $forms) {
                           ?>
                           <?php if($forms['type'] == 'text' || $forms['type'] == 'password' || $forms['type'] == 'email') {
                             ?>
                             <div class="form-group">
                                 <input type="<?php echo $forms['type'];?>" class="form-control" name="<?php echo $forms['name'];?>" placeholder="<?php echo $forms['placeholder'];?>" required="<?php echo $forms['required'];?>">
                             </div>
                           <?php } else if($forms['type'] == 'select'){
                             ?>
                             <div class="form-group">
                               <label><?php echo $forms['placeholder'];?></label>
                                 <select class="" style="width: 100%" id="<?php echo $forms['id'];?>" name="<?php echo $forms['name'];?>">
                                   <?php if($forms['op'] == 'C') {
                                     foreach($forms['value'] as $key => $value) {
                                       ?>
                                       <option value="<?php echo $value['country_id'];?>"><?php echo $value['country_name'];?></option>
                                     <?php } ?>
                                   <?php } else if($forms['op'] == 'T') {
                                     foreach($forms['value'] as $key => $value) {
                                       ?>
                                       <option value="<?php echo $key;?>"><?php echo $value;?></option>
                                     <?php }
                                   } ?>
                                 </select>
                             </div>
                          <?php } else if($forms['type'] == 'textarea') {
                            ?>
                            <div class="form-group">
                              <textarea class="form-control" id = "<?php echo $forms['id'];?>" name = "<?php echo $forms['name'];?>" placeholder="<?php echo $forms['placeholder'];?>"></textarea>
                            </div>
                          <?php } ?>
                         <?php } ?>

                           <div class="form-group">
                               <button type="submit" name="register" class="btn confer-btn btn-lg btn-block login-btn">Login</button>
                           </div>
                           <div class="form-group">
                               <button type="reset" name="reset" class="btn confer-btn btn-lg btn-block login-btn">Reset</button>
                           </div>
                       </form>
                   </div>
                   <div class="modal-footer">
                       <a href="#">Need Help?</a>
                   </div>
                                   </div>
           </div>
       </div>
   </div>
   <!-- End Modal HTML -->
