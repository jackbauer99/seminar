<?php

defined('BASEPATH') OR exit('No direct script access allowed');

?>

<!-- Our Speakings Area Start -->
<section class="our-speaker-area bg-img bg-gradient-overlay section-padding-100-60" style="background-image: url(<?php echo base_url();?>assets/landing_pagev2/img/bg-img/3.jpg);">
    <div class="container">
        <div class="row">
            <!-- Heading -->
            <div class="col-12">
                <div class="section-heading text-center wow fadeInUp" data-wow-delay="300ms">
                    <p>Our Speakings</p>
                    <h4>Who’s speaking</h4>
                </div>
            </div>
        </div>

        <div class="row">
            <?php foreach($speakers as $speaker) {
                ?>
            
            <!-- Single Speaker Area -->
            <div class="col-12 col-md-6 col-lg-4">
                <div class="single-speaker-area bg-gradient-overlay-2 wow fadeInUp" data-wow-delay="300ms">
                    <!-- Thumb -->
                    <div class="speaker-single-thumb">
                        <img src="<?php echo base_url();?>assets/image/Speakers/<?php echo $speaker['speakers_image'];?>" alt="">
                    </div>
                    <!-- Info -->
                    <div class="speaker-info">
                        <h5><?php echo $speaker['speakers_name'];?></h5>
                        <p style="color: white"><?php echo $speaker['speakers_job'];?></p>
                    </div>
                </div>
            </div>
    <?php   } ?>   
        </div>
    </div>
</section>
<!-- Our Speakings Area End -->
