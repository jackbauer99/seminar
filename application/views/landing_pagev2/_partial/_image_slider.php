<?php

defined('BASEPATH') OR exit('No direct script access allowed');

?>

            <!-- Single Slide -->
            <?php foreach($image as $images) {
                ?>
            <div class="single-welcome-slide bg-img bg-overlay jarallax" style="background-image: url(<?php echo base_url();?>assets/image/SliderImage/<?php echo $images['front_image_slider_image'];?> );">
                <div class="container h-100">
                    <div class="row h-100 align-items-center">
                        <!-- Welcome Text -->
                        <div class="col-12">
                            <div class="welcome-text-two text-center">
                                <?php foreach($period as $periods) {
                                    ?>
                                <h2 data-animation="fadeInUp" data-delay="100ms"><?php echo $periods['periode_name'];?></h2>
                                <!-- Event Meta -->
                                <div class="event-meta" data-animation="fadeInUp" data-delay="500ms">
                                    <a class="event-date" href="#"><i class="zmdi zmdi-alarm-check"></i> <?php echo $this->date->getDate('ENG',$periods['periode_finish']);?> - <?php echo $periods['periode_method'];?></a>
                                </div>
                                <?php } ?>
                                <div class="hero-btn-group" data-animation="fadeInUp" data-delay="700ms">
                                    <a href="#myModal" data-toggle="modal" class="btn trigger-btn confer-btn m-2">CONFERENCE REGISTER <i class="zmdi zmdi-long-arrow-right"></i></a>
                                    <a href="https://drive.google.com/file/d/1wK5ZuN8wZ9DKpXyMxRr_UzjyxapMfUU-/view" class="btn confer-btn m-2">GET PAPER TEMPLATE <i class="zmdi zmdi-long-arrow-right"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
         <?php  } ?>

        </div>

        <!-- Scroll Icon -->
        <div class="icon-scroll" id="scrollDown"></div>
    </section>
    <!-- Welcome Area End -->
