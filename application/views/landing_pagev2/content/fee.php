<?php

defined('BASEPATH') OR exit('No direct script access allowed');

?>

      <!-- Breadcrumb Area Start -->
      <section class="breadcrumb-area bg-img bg-gradient-overlay jarallax" style="background-image: url(img/bg-img/27.jpg);">
          <div class="container h-100">
              <div class="row h-100 align-items-center">
                  <div class="col-12">
                      <div class="breadcrumb-content">
                          <h2 class="page-title">Fee</h2>
                      </div>
                  </div>
              </div>
          </div>
      </section>
      <!-- Breadcrumb Area End -->

      <!-- Our Ticket Pricing Table Area Start -->
      <section class="our-ticket-pricing-table-area section-padding-100-0">
          <div class="container">
              <div class="row">
                <?php foreach($row as $rows) {
                  ?>
                  <!-- Single Ticket Pricing Table -->
                  <div class="col-12 col-lg-4">
                      <div class="single-ticket-pricing-table style-2 text-center mb-100 wow fadeInUp" data-wow-delay="300ms">
                          <h6 class="ticket-plan"><?php echo $rows['authors_category'];?></h6>
                          <!-- Ticket Icon -->
                          <!-- <div class="ticket-icon">
                              <img src="img/core-img/p1.png" alt="">
                          </div> -->
                          <h6>Early Bird : <?php echo $rows['EB'];?></h6>
                          <h6>Regular : <?php echo $rows['R'];?></h6>
                      </div>
                  </div>
                <?php } ?>
                <!-- Single Ticket Pricing Table -->
                <div class="col-12 col-lg-4">
                    <div class="single-ticket-pricing-table style-2 text-center mb-100 wow fadeInUp" data-wow-delay="300ms">
                        <h6 class="ticket-plan">Additional Pages</h6>
                        <!-- Ticket Icon -->
                        <!-- <div class="ticket-icon">
                            <img src="img/core-img/p1.png" alt="">
                        </div> -->
                        <h6>Early Bird : <?php echo 'Rp'.number_format(150000);?></h6>
                        <h6>Regular : <?php echo 'Rp'.number_format(150000);?></h6>
                    </div>
                </div>
                </div>
              </div>
            </section>
