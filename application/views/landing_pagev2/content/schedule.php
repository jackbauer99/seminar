<?php

defined('BASEPATH') OR exit('No direct script access allowed');

?>

<!-- Breadcrumb Area Start -->
<section class="breadcrumb-area bg-img bg-gradient-overlay jarallax" style="background-image: url(img/bg-img/17.jpg);">
    <div class="container h-100">
        <div class="row h-100 align-items-center">
            <div class="col-12">
                <div class="breadcrumb-content">
                    <h2 class="page-title">Important Dates</h2>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Breadcrumb Area End -->

<!-- Our Schedule Area Start -->
<section class="our-schedule-area bg-white section-padding-100">
    <div class="container">
      <div class="row">
          <div class="col-12">
              <div class="schedule-tab">
                  <!-- Nav Tabs -->
                  <h3>Important Dates</h3>
              </div>

<!-- Tab Content -->
<div class="tab-content" id="conferScheduleTabContent">
    <div class="tab-pane fade show active" id="step-one" role="tabpanel" aria-labelledby="monday-tab">
        <!-- Single Tab Content -->
        <div class="single-tab-content">
            <div class="row">
                <div class="col-12">
                  <?php foreach($row as $rows) {
                  ?>
                    <!-- Single Schedule Area -->
                    <div class="single-schedule-area single-page d-flex flex-wrap justify-content-between align-items-center wow fadeInUp" data-wow-delay="300ms">
                        <!-- Single Schedule Thumb and Info -->
                        <div class="single-schedule-tumb-info d-flex align-items-center">
                            <!-- Single Schedule Info -->
                            <div class="single-schedule-info">
                                <h6><?php echo $rows['schedule_name'];?></h6>
                            </div>
                        </div>
                        <!-- Single Schedule Info -->
                        <div class="schedule-time-place">
                            <p><i class="zmdi zmdi-time"></i><?php echo $this->date->getDate('ENG',$rows['schedule_time']);?></p>
                            <!-- <p><i class="zmdi zmdi-map"></i> Mountain Resort, Phoenix, USA</p> -->
                        </div>
                    </div>
                  <?php } ?>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      </section>
