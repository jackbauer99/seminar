<?php

defined('BASEPATH') OR exit('No direct script access allowed');

?>

<!-- Breadcrumb Area Start -->
    <section class="breadcrumb-area bg-img bg-gradient-overlay jarallax" style="background-image: url(img/bg-img/37.jpg);">
        <div class="container h-100">
            <div class="row h-100 align-items-center">
                <div class="col-12">
                    <div class="breadcrumb-content">
                        <h2 class="page-title"><?php echo $subtitle;?></h2>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Breadcrumb Area End -->


<!-- Contact Us Area Start -->
    <section class="contact--us-area section-padding-100-0">
        <div class="container">
            <div class="row">
                <!-- Contact Us Thumb -->
                <div class="col-12">
                    <div class="contact-us-thumb mb-100">
                        <img src="img/bg-img/44.jpg" alt="">
                    </div>
                </div>

                <!-- Contact Form -->
                <div class="col-12">
                    <div class="contact_from_area mb-100 clearfix">
                        <!-- Contact Heading -->
                        <div class="contact-heading">
                            <h4><?php echo $text;?></h4>
                            <p></p>
                        </div>
                        <div class="contact_form">
                            <form action="<?php echo base_url($url);?>" method="post" enctype = "multipart/form-data">
                            	<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>"
                         value="<?php echo $this->security->get_csrf_hash(); ?>">
                                <div class="contact_input_area">
                                    <div class="row">
                                    	<?php foreach($form as $forms) {
                                    		if($forms['type'] == 'text' || $forms['type'] == 'email' || $forms['type'] == 'password') {
                                    			?>
                                    			<!-- Form Group -->
                                        <div class="col-12 col-lg-<?php echo $forms['size'];?>">
                                            <div class="form-group">
                                                <input type="<?php echo $forms['type'];?>" class="form-control mb-30" name="<?php echo $forms['name'];?>" id="<?php echo $forms['id'];?>" placeholder="<?php echo $forms['placeholder'];?>" value = "<?php echo $forms['value'];?>" <?php echo ($forms['readonly']) ? "readonly" : null;?> <?php echo ($forms['required']) ? " required" : null;?>>
                                            </div>
                                        </div>
                                    	<?php	} else if($forms['type'] == 'select') {
                                    		?>
                                    		<!-- Form Group -->
                                        <div class="col-12 col-lg-<?php echo $forms['size'];?>">
                                            <div class="form-group">
                                                <select id = "<?php echo $forms['id'];?>" class="form-control" name = "<?php echo $forms['name'];?>" <?php echo ($forms['multiple']) ? "multiple = multiple" : null;?>>
                                                	<option value="">--- <?php echo $forms['label'];?> ---</option>
                                                	<?php foreach($forms['value'] as $key => $value) {
                                                		?>
                                                		<option value="<?php echo $key;?>"><?php echo $value;?></option>
                                                <?php } ?>
                                            </select>
                                            </div>
                                        </div>
                                    <?php	} else if($forms['type'] == 'file') {
                                    	?>
                                    	<!-- Form Group -->
                                        <div class="col-12 col-lg-<?php echo $forms['size'];?>">
                                            <div class="form-group">
                                                <input type="<?php echo $forms['type'];?>" class="form-control mb-30" name="<?php echo $forms['name'];?>" id="<?php echo $forms['id'];?>" placeholder="<?php echo $forms['placeholder'];?>" value = "<?php echo $forms['value'];?>">
                                            </div>
                                        </div>    	
                                <?php  }
                                    		
                                    } ?>
                                        <!-- Button -->
                                        <div class="col-12">
                                        	<center>
                                            	<button type="submit" class="btn confer-btn">Confirmation <i class="zmdi zmdi-long-arrow-right"></i></button>
                                            </center>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Contact Us Area End -->