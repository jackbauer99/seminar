<?php

defined('BASEPATH') OR exit('No direct script access allowed');

?>

<!-- Bootstrap core JavaScript -->
<script src="<?php echo base_url();?>assets/landing_page/vendor/jquery/jquery.min.js"></script>
<script src="<?php echo base_url();?>assets/landing_page/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

</body>

</html>
