<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Date {

	private $ci;

	public function __construct() {
      $this->ci =& get_instance();
    }

    public function getMonthNameENGarray() {
    	$month[1] = 'January';
    	$month[2] = 'February';
    	$month[3] = 'March';
    	$month[4] = 'April';
    	$month[5] = 'May';
    	$month[6] = 'June';
    	$month[7] = 'July';
    	$month[8] = 'August';
    	$month[9] = 'Septembre';
    	$month[10] = 'October';
    	$month[11] = 'November';
    	$month[12] = 'December';
    	return $month;
    }

    public function ordinal($number) {
    	$ends = array('th','st','nd','rd','th','th','th','th','th','th');
	    if ((($number % 100) >= 11) && (($number%100) <= 13))
	        return $number. 'th';
	    else
	        return $number. $ends[$number % 10];
    }

    public function getDate($lang,$data) {
    	$ex = explode('-',$data);
    	if($lang == 'ENG') {
    		$month = $this->getMonthNameENGarray();
    		return $month[(int) $ex[1]].' '.$this->ordinal((int) $ex[2]).', '.$ex[0];
    	}
    }
}