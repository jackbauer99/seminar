<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class CustomUpload {

	private $ci;

	public function __construct() {
      $this->ci =& get_instance();
    }


	public function UploadImage($name,$namefile = null,$dir) {

		$config['upload_path'] = './assets/image/'.$dir.'/';
		$config['allowed_types'] = '*';
		if($namefile != null) {
			$ext = explode('.',$_FILES[$name]['name']);
			$x = strtolower(end($ext));
			$config['file_name'] = "$namefile";	
		} else if($namefile == null) {
		 	$config['file_name'] = $config['upload_path'].'default.jpg';
		}
		$config['overwrite'] = true;
		$config['max_size'] = 5000;
		$this->ci->load->library('upload',$config);
		if($this->ci->upload->do_upload($name)) {
			return $this->ci->upload->data("file_name");
		} else {
			$error['status'] = true;
			$error['messages'] = $this->ci->upload->display_errors();
			return $error;
		}
	}
	
}