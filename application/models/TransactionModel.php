<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class TransactionModel extends CI_Model {

	public function SumTransaction($regis_type,$additional,$code) {

		$total = 0;
		if(!empty($additional)) {
			foreach($additional as $key => $value) {
				$total += $this->Users->AdditionalCostPrice($value);
			}
		}

		$price = $this->Users->AuthorsPrice1($regis_type,$code);

		$total += $price['total_price'];

		return $total;
	}

	public function AddTransaction($authors_id, $regis_type, $additional, $total) {

		$data = array(
			'transaction_autors' => $authors_id,
			'transaction_type' => $regis_type,
			'transaction_additional' => $additional,
			'transaction_total' => $total,
			'is_delete' => 0,
			'is_pay' => 0,
			'transaction_pay_date' => null
		);

		$insert = $this->db->insert('transaction_regis',$data);

		return $insert;

	}

	public function UpdateTransaction($authors_id,$regis_type,$authors_before,$price_type,$total_before) {

		$total = $total_before - $authors_before + $price_type;

		$status =  array(
			'transaction_autors' => $authors_id
		);
		$data = array(
			'transaction_type' => $regis_type,
			'transaction_total' => $total,
			'is_delete' => 0,
			'transaction_pay_date' => null,
			'is_pay' => 0,
		);

		$this->db->where($status);
		$update = $this->db->update('transaction_regis',$data);

		return $update;

	}

	public function getTransactionAuthors($id) {

		$sql = "SELECT*FROM transaction_regis WHERE transaction_autors = $id";
		$query = $this->db->query($sql)->row_array();
		return $query;

	}
}
