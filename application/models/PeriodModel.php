<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class PeriodModel extends CI_Model {

	public function column() {
		$column  = array('periode_name','periode_theme','periode_start','periode_finish','periode_earlybird','periode_regular','periode_method','periode_seats');
		return $column;
	}

	public function table() {
		$table = array('No','Period Name','Period Theme','Periode Start','Period Finish','Period Early','Period Regular','Period Method','Period Seats','Action');
		return $table;
	}

	public function id() {
		return 'periode_id';
	}

	public function comboPeriod() {

		$sql = "SELECT periode_id, periode_name FROM periode WHERE is_delete = 0";
		$query = $this->db->query($sql)->result_array();
		return $this->arrayquery->arrQuery($query);
	}

	public function PeriodAllActive() {
		$sql = "SELECT*FROM periode WHERE is_active = 0 AND is_delete = 0";
		$query = $this->db->query($sql)->result_array();
		return $query;
	}

  public function PeriodActive() {
    $sql = "SELECT*FROM periode WHERE is_active = 0 AND is_delete = 0";
    $query = $this->db->query($sql)->row_array();
    return $query;
  }

	public function PeriodAll() {
		$sql = "SELECT*FROM periode WHERE is_delete = 0";
		$query = $this->db->query($sql)->result_array();
		return $query;
	}

	public function PeriodGet($id) {
		$sql = "SELECT*FROM periode WHERE is_delete = 0 AND periode_id = $id";
		$query = $this->db->query($sql)->row_array();
		return $query;
	}

	public function PeriodDelete($id) {
		$rs = $this->db->get_where('periode',array('periode_id' => $id))->row_array();
		if($rs['is_active'] == 0) {
			return 'failed';
		} else if($rs['is_active'] == 1) {
			$this->db->where('periode_id',$id);
			$delete = $this->db->update('periode',array('is_delete' => 1));
			return $delete;
		}
	}

	public function PeriodSettingsAct() {

    $post = $this->input->post();
    $data = array(
      'periode_name' => $post['periodname'],
      'periode_theme' => $post['periodtheme'],
      'periode_start' => $post['periodstart'],
      'periode_finish' => $post['periodfinish'],
      'periode_method' => $post['periodmethod'],
      'periode_seats' => $post['periodseats'],
			'periode_earlybird' => $post['periodearly'],
			'periode_regular' => $post['periodregular'],
      'is_active' => $post['status'],
      'is_delete' => 0
    );
    $status = array(
          'is_active' => 0,
          'is_delete' => 0
        );
    $this->db->where($status);
    $count = $this->db->count_all('periode');
    if($count > 0) {
      if($post['status'] == 0) {
        $status = array(
          'is_active' => 1
        );
        $this->db->where('is_active',0);
        $query = $this->db->update('periode',$status);
        if($query) {
          $insert = $this->db->insert('periode',$data);
        }
      } else if($post['status'] == 1) {
          $insert = $this->db->insert('periode',$data);
      }
    } else if($count < 1) {
        $insert = $this->db->insert('periode',$data);
    }

    return $insert;
  }

	public function PeriodSettingsUpdAct($id) {

		$post = $this->input->post();
		$data = array(
			'periode_name' => $post['periodname'],
			'periode_theme' => $post['periodtheme'],
			'periode_start' => $post['periodstart'],
			'periode_finish' => $post['periodfinish'],
			'periode_method' => $post['periodmethod'],
			'periode_seats' => $post['periodseats'],
			'periode_earlybird' => $post['periodearly'],
			'periode_regular' => $post['periodregular'],
			'is_active' => $post['status'],
			'is_delete' => 0
		);
		$where = array(
			'periode_id' => $id
		);
		$this->db->where($where);
		$query = $this->db->update('periode',$data);

		return $query;
	}

  public function PeriodActivated($id) {

  	$status = array(
      'is_active' => 0,
      'is_delete' => 0
    );
    $this->db->where($status);
    $update = $this->db->update('periode',array('is_active' => 1));
    if($update) {
    	$this->db->where('periode_id',$id);
    	$query = $this->db->update('periode',array('is_active' => 0));
    }
    return $query;
  }

  public function PeriodDeactivated($id) {

  	$status = array(
  		'is_active' => 0,
  		'is_delete' => 0
  	);

  	$this->db->where($status);
  	$count = $this->db->count_all('periode');
  	if($count < 1) {
  		return 'failed';
  	} else if($count == 1) {
  		$this->db->where('periode_id',$id);
  		$update = $this->db->update('periode',array('is_active' => 1));
  		return $update;
  	}
  }
}
