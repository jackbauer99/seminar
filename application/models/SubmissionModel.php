<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class SubmissionModel extends CI_Model {


	public function BaseTable() {

		$table = array('No','Title','Authors Name','Submission File','Action');
		return $table;
	}

	public function BaseColumn() {

		$column = array('submission_title','name','submission_file');
		return $column;
	}

	public function BaseQuery() {

		$sql = "SELECT submission_id,submission_title, CONCAT(authors_first_name,' ',authors_last_name) AS name, submission_file, submission_decission FROM submission LEFT JOIN authors ON submission_authors = authors_id WHERE submission.is_delete = 0";
		$query = $this->db->query($sql)->result_array();
		return $query;

	}

	public function BaseID() {

		return "submission_id";
	}

	public function SubmissionAll() {

		$rows = array();
		$data = $this->BaseQuery();
		foreach($data as $key => $value) {
			if($value['submission_file']) {
				$value['submission_file'] = "<a href = ".base_url()."assets/image/Submission/".$value['submission_file'].">$value[submission_file]</a>";
			}
			$rows[] = $value;
		}

		return $rows;
	}

	public function getSubmission($id) {

		$where = array(
			'submission_id' => $id,
			'is_delete' => 0
		);
		$this->db->where($where);
		$submission = $this->db->get('submission')->row_array();
		return $submission;

	}

	public function SubmissionApprove($id) {

		$status = array(
			'submission_decission' => 1
		);

		$this->db->where('submission_id',$id);
		$update = $this->db->update('submission',$status);

		return $update;

	}

	public function SubmissionDenied($id) {

		$status = array(
			'submission_decission' => 2
		);

		$this->db->where('submission_id',$id);
		$update = $this->db->update('submission',$status);

		return $update;

	}

	public function CountSubmission($act = null, $period = null) {

		$sql = "SELECT COUNT(submission_id) AS total FROM submission JOIN authors ON submission_authors = authors_id WHERE submission.is_delete = 0 ";
		if(!empty($period)) {
			$sql .= " AND authors_period = $period";
		}
		if(!empty($act)) {
			$sql .= " AND submission_decission = $act";
		}

		$query = $this->db->query($sql)->row_array();
		return $query;
	}

}
