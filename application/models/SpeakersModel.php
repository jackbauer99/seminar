<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class SpeakersModel extends CI_Model {

	public function column() {
		$column = array('speakers_name','speakers_job','periode_name');
		return $column;
	}

	public function table() {
		$table = array('No','Speakers Name','Speakers Job','Speakers Period','Action');
		return $table;
	} 

	public function id() {
		return 'speakers_id';
	}

	public function SpeakersAll() {
		$sql = "SELECT*FROM speakers a INNER JOIN periode b ON a.speakers_period = b.periode_id WHERE a.is_delete = 0";
		$query = $this->db->query($sql)->result_array();
		return $query;
	}

	public function SpeakersAllActive() {
		$sql = "SELECT*FROM speakers WHERE is_active = 0 AND is_delete = 0";
		$query = $this->db->query($sql)->result_array();
		return $query;
	}

	public function SpeakersPeriod($period) {
		$sql = "SELECT*FROM speakers WHERE is_active = 0 AND is_delete = 0 AND speakers_period = $period";
		$query = $this->db->query($sql)->result_array();
		return $query;	
	}

	public function SpeakersCount($period = null) {
		$sql = "SELECT COUNT(speakers_id) AS total FROM speakers ";
		if(!empty($period)) {
			$sql .= " WHERE is_active = 0 AND is_delete = 0 AND speakers_period = $period";
		}
		$query = $this->db->query($sql)->row_array();
		return $query;
	}

	public function SpeakersSettingsAct() {

		$post = $this->input->post();
	    $upload = $this->customupload->UploadImage('speakersimage','S-'.md5(rand(1,100)),'Speakers');
	    if($upload == 'failed') {
	      return 'failed';
	    }
	    $period = $this->Period->PeriodActive();
	    $data = array(
	    	'speakers_name' => $post['speakersname'],
	    	'speakers_job' => $post['speakersjob'],
	    	'speakers_image' => $upload,
	    	'speakers_period' => $period['periode_id'],
	    	'is_active' => $post['status'],
	    	'is_delete' => 0
	    );
	    $insert = $this->db->insert('speakers',$data);
	    return $insert;
	}
}