<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class CountryModel extends CI_Model {

  public function CountryAll() {

    $sql = "SELECT country_id, country_name FROM country";
    $data = $this->db->query($sql)->result_array();
    return $data;

  }

  public function CountryName($id) {

  	$sql = "SELECT country_name FROM country WHERE country_id = $id";
  	$data = $this->db->query($sql)->row_array();
  	return $data['country_name'];

  }
}
