<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class UsersModel extends CI_Model {

  // public function AuthorsValidation() {
  //
  //   $this->load->library('form_validation');
  //   $this->form_validat
  // }

  const authors = 'authors_id';
  const type_regis = 'id_type';

  public function BaseDetailColumn() {

    $column = array('name','authors_email','country_name','authors_address','authors_phone_number','authors_type','authors_student_number','periode_name','transaction_additional','transaction_total','transaction_pay_date','is_pay');

    return $column;
  }

  public function BaseDetailTable() {

    $table = array('Authors Name','Authors Email','Country Origin','Authors Address','Authors Phone Number','Authors Type','Authors Student Number','Period','Additional Transaction','Transaction Total','Pay Date','Status');

    return $table;
  }

  public function BaseColumn() {

    $column = array('name','authors_email','country_name','authors_address','authors_phone_number','authors_type');

    return $column;
  }

  public function BaseTable() {

    $table = array('No','Authors Name','Authors Email','Country Origin','Authors Address','Authors Phone Number','Authors Type','Action');

    return $table;
  }

  public function BaseID($name = null) {
    if($name == 'type_regis') {
      return 'id_type';
    } else if($name == 'authors') {
      return 'authors_id';
    } else if($name == 'price') {
      return 'id_price';
    }
  }

  public function BaseName() {
    return 'name';
  }

  public function AuthorsType($authors = null) {

    $sql = "SELECT*FROM type_regis ";
    if($authors != null) {
      $sql .= "WHERE id_type = $authors";
      $query = $this->db->query($sql)->row_array();
      return $query['name_type'];
    } else {
      $query = $this->db->query($sql)->result_array();
      return $query;
    }

  }

  public function AuthorsTypeCombo() {

    $sql = "SELECT*FROM type_regis ";
    $query = $this->db->query($sql)->result_array();
    return $this->arrayquery->arrQuery($query);

  }

  public function TableAuthorsType() {

    $table = array('No','Type Name','Action');
    return $table;

  }

  public function ColumnAuthorsType() {

    $column = array('name_type');
    return $column;

  }

  public function AuthorsTypeAct() {

    $post = $this->input->post();
    $data = array(
      'name_type' => $post['name_type']
    );
    $insert = $this->db->insert('type_regis',$data);

    return $insert;
  }

  public function TablePriceType() {

    $table = array('No','Category','Early Bird','Regular');
    return $table;

  }

  public function ColumnPriceType() {

    $table = array('authors_category','EB','R');
    return $table;

  }

  public function PriceStatus() {

    $data['EB'] = 'Early Bird';
    $data['R'] = 'Regular';
    return $data;

  }

  public function AuthorsPrice1($authors = null, $code = null) {

    $rows = array();
    $rows2 = array();
    $sql = "SELECT*FROM price";
    if(!empty($authors)) {
        $sql .= " WHERE type_price = $authors AND status_price = '$code'";
        $query =  $this->db->query($sql)->row_array();
        // $this->maintence->Debug($query['total_price']);
        return $query;
    } else {
      $query =  $this->db->query($sql)->result_array();
      foreach($query as $key => $values) {
        // if(empty($values['status_price'])) {
        //   $rows[$this->AuthorsType($values['type_price'])][$values['status_price']] = 0;
        // } else {
          $rows[$this->AuthorsType($values['type_price'])][$values['status_price']] = 'Rp. '.number_format($values['total_price']);
        //}
      }

      foreach($rows as $key => $val) {

        if(empty($val['EB'])) {
          $val['EB'] = 'Rp. '.number_format(0);
        }

        if(empty($val['R'])) {
          $val['R'] = 'Rp. '.number_format(0);
        }

        $val['authors_category'] = $key;

        $rows2[] = $val;
      }

      return $rows2;
    }

    //$this->maintence->Debug($rows2[1]['EB']);
  }

  public function AuthorsPriceAct() {

    $post = $this->input->post();
    $period = $this->Period->PeriodActive();
    $data = array(
      'type_price' => $post['authors_type'],
      'total_price' => $post['total'],
      'period_price' => $period['periode_id'],
      'status_price' => $post['status_price']
    );
    $sql = "SELECT*FROM price";
    $query = $this->db->query($sql)->result_array();
    foreach($query as $key => $value) {
      if($value['type_price'] == $post['authors_price'] && $value['period_price'] == $post['periode_id'] && $value['status_price'] == $post['status_price']) {
        $msg = 'warning';
        return $msg;
      }
    }
    $insert = $this->db->insert('price',$data);
    return $insert;
  }

  public function AuthorsAll() {

    $sql = "SELECT authors_id,CONCAT(authors_first_name,' ',authors_last_name) AS name, authors_email, country_name,
            authors_address, authors_phone_number, authors_type, authors_student_number, periode_name,
            transaction_additional, transaction_total, transaction_pay_date, is_pay FROM authors JOIN country ON
            authors_country = country_id JOIN periode ON authors_period = periode_id LEFT JOIN transaction_regis
            ON authors_id = transaction_autors WHERE authors.is_delete = 0";
    $query = $this->db->query($sql)->result_array();
    return $query;
  }

  public function CountAuthors($period = null) {

    $sql = "SELECT COUNT(authors_id) AS total FROM authors ";
    if(!empty($period)) {
      $sql .= "WHERE is_delete = 0 AND authors_period = $period";
    }
    $query = $this->db->query($sql)->row_array();
    return $query;

  }

  public function getAuthors($id) {

    $where = array(
      'authors_id' => $id,
      'is_delete' => 0
    );
    $this->db->where($where);
    $authors = $this->db->get('authors')->row_array();
    return $authors;

  }

  public function DetailAuthorsAll() {

    $rows2 = array();
    $rows = $this->AuthorsAll();
    foreach($rows as $key => $value) {
      if($value['transaction_additional'] == '0,1') {
          $value['transaction_additional'] = $this->utils->explode2($value['transaction_additional']);
      } else if($value['transaction_additional'] == 0) {
        $value['transaction_additional'] = $this->Users->AdditionalCost($value['transaction_additional']);
      } else if($value['transaction_additional'] == 1) {
        $value['transaction_additional'] = $this->Users->AdditionalCost($value['transaction_additional']);
      }

      if($value['authors_type']) {
        $value['authors_type'] = $this->AuthorsType($value['authors_type']);
      }
      if($value['is_pay'] == 0) {
          $value['is_pay'] = 'Not yet paid';
      } else if($value['is_pay'] == 1) {
          $value['is_pay'] = 'Paid';
      }
      if($value['country_name'] != 'Indonesia') {
        $value['transaction_total'] = '$.'.$this->utils->IDRtoDollar($value['transaction_total']);
      } else {
        $value['transaction_total'] = 'Rp.'.number_format($value['transaction_total']);
      }

      $rows2[] = $value;

    }

    //$this->maintence->Debug($rows2);

    return $rows2;
  }

  public function AdditionalCost($cost = null) {

    $data[0] = 'Proceeding Book';
    $data[1] = 'Tour';
    if($cost != null) {
      return $data[$cost];
    } else {
      return $data;
    }

  }

  public function AdditionalCostPrice($cost = null) {

    $data[0] = 70000;
    $data[1] = 1500000;
    if($cost != null) {
      return $data[$cost];
    } else {
      return $data;
    }

  }

  public function regisTemp($codegenarate) {

    $this->db->where('regis_token',$codegenarate);
    $where = $this->db->get('temp_regis')->row_array();
    return $where;

  }

  public function AuthorsTempID($id) {

    $this->db->where('authors_regis_id',$id);
    $where = $this->db->get('authors')->row_array();
    return $where['authors_id'];

  }

  public function AuthorsRegisterTemp($codegenarate) {

    $post = $this->input->post();
    $period = $this->Period->PeriodActive();
    $insert = array(
      'regis_first_name' => ucfirst($post['firstname']),
      'regis_last_name' => ucfirst($post['lastname']),
      'regis_email' => strtolower($post['email']),
      'regis_country' => $post['country'],
      'regis_address' => $post['address'],
      'regis_phone_number' => $post['phonenumber'],
      'regis_type' => $post['authors_type'],
      'regis_period' => $period['periode_id'],
      'regis_token' => $codegenarate,
      'is_delete' => 0,
      'is_update' => 0
    );
    $insert = $this->db->insert('temp_regis',$insert);
    return $insert;
  }

  public function AuthorsRegister($codegenarate,$code) {

    $post = $this->input->post();
    $regis = $this->regisTemp($codegenarate);
    if(!empty($post['cost'])) {
      $imp = implode(',',$post['cost']);
    } else {
      $imp = null;
    }
    if($regis['regis_type'] == 2) {

        $organization = $post['organization'];
        $student_id = $post['npm'];

      } else {

        $organization = null;
        $student_id = null;

      }
    $data = array(
      'authors_first_name' => ucfirst($post['firstname']),
      'authors_last_name' => ucfirst($post['lastname']),
      'authors_email' => $regis['regis_email'],
      'authors_country' => $regis['regis_country'],
      'authors_address' => $regis['regis_address'],
      'authors_phone_number' => $regis['regis_phone_number'],
      'authors_organization' => $organization,
      'authors_type' => $regis['regis_type'],
      'authors_student_number' => $student_id,
      'authors_period' => $regis['regis_period'],
      'authors_regis_id' => $regis['regis_id'],
      'is_delete' => 0,
    );

    $insert = $this->db->insert('authors',$data);

    $authors_id = $this->AuthorsTempID($regis['regis_id']);

    if($insert) {


      if($regis['regis_type'] == 1 || $regis['regis_type'] == 2) {


      $upload = $this->customupload->UploadImage('file','S-'.md5(rand(1,100)),'Submission');
      if($upload == 'failed') {
        return 'failed';
      }
      $insert_subs = array(
        'submission_title' => $post['titlesubs'],
        'submission_authors' => $authors_id,
        'submission_file' => $upload,
        'submission_decission' => 0,
        'is_delete' => 0
      );

      $insert_sub = $this->db->insert('submission',$insert_subs);
    }

    // if($regis['regis_type'] != 1 || $regis['regis_type'] != 2) {

      $total = $this->Transaction->SumTransaction($regis['regis_type'],@$post['cost'],$code);

      $insert_transac = $this->Transaction->AddTransaction($authors_id,$regis['regis_type'],$imp,$total);

    // }

  }

    return $insert;
  }

  public function AuthorsUpdateAct($id) {

    $status = array(
      'authors_id' => $id
    );
    $data = array(
      'authors_type' => 3
    );
    $this->db->where($status);
    $update = $this->db->update('authors',$data);
    return $update;

  }

  public function UsersRegister($random,$codegenerate) {

    $post = $this->input->post();
    $data = array(
      'users_name' => $post['email'],
      'users_password' => $random,
      'is_login' => 0,
      'is_active' => 0,
      'url_token' => $codegenerate
    );

    $insert = $this->db->insert('users',$data);

    return $insert;
  }

  public function UpdateTempRegisIfFailed($codegenarate) {

    $status = array(
      'is_update' => 0
    );
    $this->db->where('regis_token',$codegenarate);
    $update = $this->db->update('temp_regis',$status);

    return $update;
  }

  public function UpdateTempRegisIfSuccess($codegenarate) {

    $status = array(
      'is_update' => 1
    );
    $this->db->where('regis_token',$codegenarate);
    $update = $this->db->update('temp_regis',$status);

    return $update;
  }
}
