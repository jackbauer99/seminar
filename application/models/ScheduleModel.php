<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class ScheduleModel extends CI_Model {

  public function BaseColumn() {
    $column = array('schedule_name','schedule_time','schedule_period');
    return $column;
  }

  public function BaseTable() {
    $table = array('No','Schedule Name','Schedule Dates','Schedule Period','Action');
    return $table;
  }

  public function BaseID() {
    return 'schedule_id';
  }

  public function getAllSchedule($period = null) {

    $sql = "SELECT*FROM schedule WHERE is_delete = 0 AND is_active = 0";
    if($period != null) {
      $sql .= " AND schedule_period = $period";
    }
    $query = $this->db->query($sql)->result_array();
    return $query;
  }

  public function ScheduleAddAct() {

    $post = $this->input->post();
    $period = $this->Period->PeriodActive();
    $data = array(
      'schedule_name' => $post['schedule_name'],
      'schedule_time' => $post['schedule_time'],
      'schedule_period' => $period['periode_id'],
      'is_active' => 0,
      'is_delete' => 0,
    );
    $insert = $this->db->insert('schedule',$data);
    return $insert;
  }
}
