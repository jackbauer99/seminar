<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class AuthModel extends CI_Model {

  public function __construct() {

    parent::__construct();
    $this->load->library('form_validation');

  }

  public function authValidation() {

    $this->form_validation->set_rules('username','Username','required');
    $this->form_validation->set_rules('password','Password','required');

    if($this->form_validation->run()) {
      return true;
    } else  {
      return false;
    }

  }

  public function authCheck($user) {

    $sql = "SELECT*FROM users WHERE users_name = '$user'";
    $data = $this->db->query($sql)->row_array();
    return $data;

  }
}
