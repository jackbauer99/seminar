<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class SettingsModel extends CI_Model {

  // private function UploadImage($name,$namefile = null,$dir) {

  //   $config['upload_path'] = './assets/image/'.$dir.'/';
  //   $config['allowed_types'] = 'gif|jpg|png|jpeg';
  //   if($namefile != null) {
  //     $config['file_name'] = $config['upload_path'].$namefile;
  //   } else if($namefile == null) {
  //     $config['file_name'] = $config['upload_path'].'default.jpg';
  //   }
  //   $config['overwrite'] = true;
  //   $config['max_size'] = 50000;
  //   $this->load->library('upload',$config);
  //   if($this->upload->do_upload($name)) {
  //     return $this->upload->data("file_name");
  //   } else {
  //     return 'failed';
  //   }

  // }

  public function BaseSettingsAct() {

    $post = $this->input->post();
    $data = array(
      'base_company_name' => $post['companyname'],
      'base_dashboard_color' => $post['dashboardcolor'],
      'base_contact_us' => $post['contact'],
      'base_email' => $post['companyemail'],
      'base_email_password' => $post['companypassword'],
      'is_active' => $post['status'],
      'is_delete' => 0
    );
    $count = $this->db->count_all('base_settings');
    if($count > 0) {
      $this->db->where('base_id',1);
      $query = $this->db->update('base_settings',$data);
    } else if($count < 1) {
        $query = $this->db->insert('base_settings',$data);
    }
    return $query;

  }

  public function BaseEmail() {
    $sql = "SELECT base_email FROM base_settings WHERE is_active = 0 AND is_delete = 0";
    $query = $this->db->query($sql)->row_array($sql);
    return $query['base_email'];
  }

  public function BaseEmailPass() {
    $sql = "SELECT base_email_password FROM base_settings WHERE is_active = 0 AND is_delete = 0";
    $query = $this->db->query($sql)->row_array($sql);
    return $query['base_email_password'];
  }

  public function FrontSettingsAct() {

    $post = $this->input->post();
    $data = array(
      'front_title' => $post['title'],
      'front_subtitle' => $post['subtitle'],
      'front_about' => $post['about'],
      'front_color' => $post['frontcolor'],
      'is_active' => $post['status'],
      'is_delete' => 0
    );
    $status = array(
          'is_active' => 0,
          'is_delete' => 0
        );
    $this->db->where($status);
    $count = $this->db->count_all('front_settings');
    if($count > 0) {
      if($post['status'] == 0) {
        $status = array(
          'is_active' => 1
        );
        $this->db->where('is_active',0);
        $query = $this->db->update('front_settings',$status);
        if($query) {
          $insert = $this->db->insert('front_settings',$data);
        }
      } else if($post['status'] == 1) {
          $insert = $this->db->insert('front_settings',$data);
      }
    } else if($count < 1) {
        $insert = $this->db->insert('front_settings',$data);
    }

    return $insert;
  }

  public function FrontContentAct() {

    $post = $this->input->post();
    $data = array(
      'front_content_title' => $post['title'],
      'front_content_content' => $post['frontcontent'],
      'is_active' => $post['status'],
      'is_delete' => 0
    );
    $status = array(
          'is_active' => 0,
          'is_delete' => 0
        );
    $this->db->where($status);
    $count = $this->db->count_all('front_content');
    if($count > 0) {
      if($post['status'] == 0) {
        $status = array(
          'is_active' => 1
        );
        $this->db->where('is_active',0);
        $query = $this->db->update('front_content',$status);
        if($query) {
          $insert = $this->db->insert('front_content',$data);
        }
      } else if($post['status'] == 1) {
          $insert = $this->db->insert('front_content',$data);
      }
    } else if($count < 1) {
        $insert = $this->db->insert('front_content',$data);
    }

    return $insert;
  }

  public function FrontContactAct() {

    $post = $this->input->post();
    $data = array(
      'front_contact_content' => $post['frontcontact'],
      'is_active' => $post['status'],
      'is_delete' => 0
    );
    $status = array(
          'is_active' => 0,
          'is_delete' => 0
        );
    $this->db->where($status);
    $count = $this->db->count_all('front_contact');
    if($count > 0) {
      if($post['status'] == 0) {
        $status = array(
          'is_active' => 1
        );
        $this->db->where('is_active',0);
        $query = $this->db->update('front_contact',$status);
        if($query) {
          $insert = $this->db->insert('front_contact',$data);
        }
      } else if($post['status'] == 1) {
          $insert = $this->db->insert('front_contact',$data);
      }
    } else if($count < 1) {
        $insert = $this->db->insert('front_contact',$data);
    }

    return $insert;
  }

  public function FrontAboutAct() {

    $post = $this->input->post();
    $data = array(
      'front_about_countdown_title' => $post['abouttitle'],
      'front_about_countdown_content' => $post['aboutcontent'],
      'is_active' => $post['status'],
      'is_delete' => 0
    );
    $status = array(
          'is_active' => 0,
          'is_delete' => 0
        );
    $this->db->where($status);
    $count = $this->db->count_all('front_about_countdown');
    if($count > 0) {
      if($post['status'] == 0) {
        $status = array(
          'is_active' => 1
        );
        $this->db->where('is_active',0);
        $query = $this->db->update('front_about_countdown',$status);
        if($query) {
          $insert = $this->db->insert('front_about_countdown',$data);
        }
      } else if($post['status'] == 1) {
          $insert = $this->db->insert('front_about_countdown',$data);
      }
    } else if($count < 1) {
        $insert = $this->db->insert('front_about_countdown',$data);
    }

    return $insert;
  }


  public function FrontHeadersAct() {

    $post = $this->input->post();
    $upload = $this->customupload->UploadImage('header','FH-'.md5(rand(1,100)),'Front');
    if($upload == 'failed') {
      return 'failed';
    }
    $data = array(
      'front_header_logo' => $upload,
      'front_header_nav_key' => 1,
      'front_header_submit_link' => $post['link'],
      'is_active' => $post['status'],
      'is_delete' => 0
    );
    $status = array(
          'is_active' => 0,
          'is_delete' => 0
        );
    $this->db->where($status);
    $count = $this->db->count_all('front_header');
    if($count > 0) {
      if($post['status'] == 0) {
        $status = array(
          'is_active' => 1
        );
        $this->db->where('is_active',0);
        $query = $this->db->update('front_header',$status);
        if($query) {
          $insert = $this->db->insert('front_header',$data);
        }
      } else if($post['status'] == 1) {
          $insert = $this->db->insert('front_header',$data);
      }
    } else if($count < 1) {
        $insert = $this->db->insert('front_header',$data);
    }

    return $insert;
  }

  public function FrontNavAct() {

    $post = $this->input->post();
    $navparents = $post['navparents'];
    if($navparents == 0) {
      $data = array(
        'front_nav_key' => 1,
        'front_nav_name' => $post['navname'],
        'front_nav_link' => $post['navlink'],
        'is_delete' => 0,
        'is_active' => 0,
        'is_parents' => $post['parent'],
        'front_nav_child' => null
      );
      $insert = $this->db->insert('front_nav',$data);
    } else {
      $sql = "SELECT*FROM front_nav WHERE front_nav_id = $navparents";
      $front = $this->db->query($sql)->row_array();

      //$this->maintence->Debug($front['front_nav_child']);

      if($front['front_nav_child'] != NULL) {
        // $count = strlen($front['front_nav_child']);
        // if($count == 0) {
        //   $frontchild = $post['navname'];
        //   $frontchildlink = $post['navlink'];
        // } else {
          $frontchild = $front['front_nav_child'].','.$post['navname'];
          $frontchildlink = $front['front_nav_child_link'].','.$post['navlink'];
        //}
        $update = array(
          'front_nav_child' => $frontchild,
          'front_nav_child_link' => $frontchildlink
        );
        $this->db->where('front_nav_id',$navparents);
        $query = $this->db->update('front_nav',$update);

        if($query) {
          $data = array(
            'front_nav_key' => 1,
            'front_nav_name' => $post['navname'],
            'front_nav_link' => $post['navlink'],
            'is_delete' => 0,
            'is_active' => 0,
            'is_parents' => 1,
            'front_nav_child' => null
      );
      $insert = $this->db->insert('front_nav',$data);
        }
      } else if($front['front_nav_child'] == NULL){
         $frontchild = $post['navname'];
         $frontchildlink = $post['navlink'];
         $update = array(
          'front_nav_child' => $frontchild,
          'front_nav_child_link' => $frontchildlink
        );
        $this->db->where('front_nav_id',$navparents);
        $query = $this->db->update('front_nav',$update);
        if($query) {
          $data = array(
            'front_nav_key' => 1,
            'front_nav_name' => $post['navname'],
            'front_nav_link' => $post['navlink'],
            'is_delete' => 0,
            'is_active' => 0,
            'is_parents' => 1,
            'front_nav_child' => null
      );
      $insert = $this->db->insert('front_nav',$data);
        }


      }
    }

    return $insert;
  }

  public function FrontImageSliderAct() {

    $post = $this->input->post();
    $upload = $this->customupload->UploadImage('sliderimage','SI-'.md5(rand(1,100)),'SliderImage');
    // if($upload['status']) {
    //   return $upload['messages'];
    // }
    $data = array(
      'front_image_slider_name' => 1,
      'front_image_slider_image' => $upload,
      'front_image_slider_name_event' => $post['sliderevent'],
      'is_active' => $post['status'],
      'is_delete' => 0
    );
    $insert = $this->db->insert('front_image_slider',$data);

    if($insert) {
      return $insert;
    } else if(!$insert) {
      @unlink(base_url().'assets/image/SliderImage/'.$upload);
    }
  }

  public function BaseSettingsAll() {

    $sql = "SELECT*FROM base_settings WHERE is_active = 0 AND is_delete = 0";
    $query = $this->db->query($sql)->result_array();
    return $query;

  }

  public function FrontSettingsAll() {

    $sql = "SELECT*FROM front_settings WHERE is_active = 0 AND is_delete = 0";
    $query = $this->db->query($sql)->result_array();
    return $query;

  }

  public function FrontHeaderAll() {

    $sql = "SELECT*FROM front_header WHERE is_active = 0 AND is_delete = 0";
    $query = $this->db->query($sql)->result_array();
    return $query;

  }

  public function FrontNavAll() {

    $sql = "SELECT*FROM front_nav WHERE is_active = 0 AND is_delete = 0 AND is_parents = 0";
    $query = $this->db->query($sql)->result_array();

    $rows = array();
    foreach($query as $key => $value) {
      if($value['front_nav_child'] != NULL) {
        $value['front_nav_child'] = explode(',',$value['front_nav_child']);
      }
      if($value['front_nav_child_link'] != NULL) {
        $value['front_nav_child_link'] = explode(',',$value['front_nav_child_link']);
      }

      $rows[] = $value;
    }
    //$this->maintence->Debug($rows);
    return $rows;

  }

  public function FrontNavParent() {

    $sql = "SELECT front_nav_id , front_nav_name FROM front_nav WHERE is_active = 0 AND is_delete = 0 AND is_parents = 0";
    $query = $this->db->query($sql)->result_array();
    return $query;

  }

  public function FrontImageSliderAll() {

    $sql = "SELECT*FROM front_image_slider WHERE is_active = 0 AND is_delete = 0";
    $query = $this->db->query($sql)->result_array();
    return $query;

  }

  public function FrontContentAll() {

    $sql = "SELECT*FROM front_content WHERE is_active = 0 AND is_delete = 0";
    $query = $this->db->query($sql)->result_array();
    return $query;

  }

  public function FrontContactAll() {

    $sql = "SELECT*FROM front_contact WHERE is_active = 0 AND is_delete = 0";
    $query = $this->db->query($sql)->result_array();
    return $query;

  }

  public function FrontAboutAll() {

    $sql = "SELECT*FROM front_about_countdown WHERE is_active = 0 AND is_delete = 0";
    $query = $this->db->query($sql)->result_array();
    return $query;

  }

}
