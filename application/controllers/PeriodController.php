<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class PeriodController extends CI_Controller {

  public function __construct() {

    parent::__construct();
    $models = array(
       'UtilsModel' => 'Utils',
       'SettingsModel' => 'Settings',
       'CountryModel' => 'Country',
       'PeriodModel' => 'Period',
       'SpeakersModel' => 'Speakers',
       'UsersModel' => 'Users',
       'SubmissionModel' => 'Submission'
    );
    foreach($models as $model => $value) {
      $this->load->model($model,$value);
    }
  }

  public function getLayout($data, $layout = null, $layout2 = null, $layout3 = null) {

    $data['base'] = $this->Settings->BaseSettingsAll();

    $this->load->view('dashboard/_partial/_header',$data);
    $this->load->view('dashboard/_partial/_sidebar',$data);
    $this->load->view('dashboard/_partial/_topbar',$data);
    if($layout != null) {
      $this->load->view('dashboard/content/'.$layout,$data);
    }
    if($layout2 != null) {
      $this->load->view('dashboard/content/'.$layout2,$data);
    }
    if($layout3 != null) {
      $this->load->view('dashboard/content/'.$layout3,$data);
    }
    $this->load->view('dashboard/_partial/_footer',$data);
    $this->load->view('dashboard/_partial/_script',$data);

  }

  public function PeriodSettings() {

    if(!$this->session->userdata('logged')) {
      redirect('gate/');
    }

    $data['title'] = 'Udin Seminar - Period Settings';
    $data['subtitle'] = 'Period Settings';
    $data['eventsettings'] = 'active';
    $data['row'] = $this->Period->PeriodAll();
    $data['editable'] = true;
    $data['url'] = 'back/periodsettingsact';
    $data['urlact'] = 'back/periodactivated/';
    $data['urldeact'] = 'back/perioddeactivated/';
    $data['urldelete'] = 'back/perioddelete/';

    $data['table'] = $this->Period->table();
    $data['id'] = $this->Period->id();
    $data['column'] = $this->Period->column();

    $data['form'][] = array('type' => 'text', 'id' => 'periodname', 'name' => 'periodname', 'placeholder' => 'Period Name', 'label' => 'Period Name', 'required' => true);
    $data['form'][] = array('type' => 'text', 'id' => 'periodtheme', 'name' => 'periodtheme', 'placeholder' => 'Period Theme', 'label' => 'Period Theme', 'required' => true);
    $data['form'][] = array('type' => 'text', 'id' => 'periodmethod', 'name' => 'periodmethod', 'placeholder' => 'Period Method', 'label' => 'Period Method', 'required' => true);
    $data['form'][] = array('type' => 'text', 'id' => 'periodseats', 'name' => 'periodseats', 'placeholder' => 'Period Seats', 'label' => 'Period Seats', 'required' => true);
    $data['form'][] = array('type' => 'date', 'id' => 'periodstart', 'name' => 'periodstart', 'placeholder' => 'Period Start', 'label' => 'Period Start', 'required' => true);
    $data['form'][] = array('type' => 'date', 'id' => 'periodfinish', 'name' => 'periodfinish', 'placeholder' => 'Period Finish', 'label' => 'Period Finish', 'required' => true);
    $data['form'][] = array('type' => 'date', 'id' => 'periodearly', 'name' => 'periodearly', 'placeholder' => 'Period Early', 'label' => 'Period Early', 'required' => true);
    $data['form'][] = array('type' => 'date', 'id' => 'periodregular', 'name' => 'periodregular', 'placeholder' => 'Period Regular', 'label' => 'Period Regular', 'required' => true);
    $data['form'][] = array('type' => 'select', 'id' => 'status', 'name' => 'status', 'placeholder' => 'Status', 'label' => 'Status', 'op' => 'F','value' => $this->Utils->isActive());

    $this->getLayout($data,'table','modal');

  }

  public function PeriodEdit($id) {

    if(!$this->session->userdata('logged')) {
      redirect('gate/');
    }

    $data['title'] = 'Udin Seminar - Period Settings';
    $data['subtitle'] = 'Period Settings';
    $data['eventsettings'] = 'active';
    $data['row'] = $this->Period->PeriodGet($id);
    $data['editable'] = true;
    $data['url'] = 'back/periodsettingsact';
    $data['url'] = 'back/periodeupdate/'.$data['row']['periode_id'];

    $data['table'] = $this->Period->table();
    $data['id'] = $this->Period->id();
    $data['column'] = $this->Period->column();

    $data['form'][] = array('type' => 'text', 'id' => 'periodname', 'name' => 'periodname', 'placeholder' => 'Period Name', 'label' => 'Period Name', 'required' => true, 'value' => $data['row']['periode_name']);
    $data['form'][] = array('type' => 'text', 'id' => 'periodtheme', 'name' => 'periodtheme', 'placeholder' => 'Period Theme', 'label' => 'Period Theme', 'required' => true, 'value' => $data['row']['periode_theme']);
    $data['form'][] = array('type' => 'text', 'id' => 'periodmethod', 'name' => 'periodmethod', 'placeholder' => 'Period Method', 'label' => 'Period Method', 'required' => true, 'value' => $data['row']['periode_method']);
    $data['form'][] = array('type' => 'text', 'id' => 'periodseats', 'name' => 'periodseats', 'placeholder' => 'Period Seats', 'label' => 'Period Seats', 'required' => true, 'value' => $data['row']['periode_seats']);
    $data['form'][] = array('type' => 'date', 'id' => 'periodstart', 'name' => 'periodstart', 'placeholder' => 'Period Start', 'label' => 'Period Start', 'required' => true, 'value' => $data['row']['periode_start']);
    $data['form'][] = array('type' => 'date', 'id' => 'periodfinish', 'name' => 'periodfinish', 'placeholder' => 'Period Finish', 'label' => 'Period Finish', 'required' => true, 'value' => $data['row']['periode_finish']);
    $data['form'][] = array('type' => 'date', 'id' => 'periodearly', 'name' => 'periodearly', 'placeholder' => 'Period Early', 'label' => 'Period Early', 'required' => true, 'value' => $data['row']['periode_earlybird']);
    $data['form'][] = array('type' => 'date', 'id' => 'periodregular', 'name' => 'periodregular', 'placeholder' => 'Period Regular', 'label' => 'Period Regular', 'required' => true, 'value' => $data['row']['periode_regular']);
    $data['form'][] = array('type' => 'select', 'id' => 'status', 'name' => 'status', 'placeholder' => 'Status', 'label' => 'Status', 'op' => 'F','value' => $this->Utils->isActive());

    $this->getLayout($data,'forms');

  }

  public function PeriodSettingsAct() {

    $post = $this->input->post();
    if(strtotime($post['periodfinish']) < strtotime($post['periodstart'])) {
      $this->session->set_flashdata('failed',ucfirst('period finish must not be less than period start'));
      redirect('back/periodsettings');
    }
    if(strtotime($post['periodearly']) > strtotime($post['periodfinish'])) {
      $this->session->set_flashdata('failed',ucfirst('period early must not be less than period finish'));
      redirect('back/periodsettings');
    }
    if(strtotime($post['periodregular']) > strtotime($post['periodfinish'])) {
      $this->session->set_flashdata('failed',ucfirst('period regular must not be less than period finish'));
      redirect('back/periodsettings');
    }
    $action = $this->Period->PeriodSettingsAct();
    if($action) {
      $this->session->set_flashdata('success',ucfirst('period settings successfully added'));
      redirect('back/periodsettings');
    } else if(!$action) {
      $this->session->set_flashdata('failed',ucfirst('period settings failed to be added'));
      redirect('back/periodsettings');
    }

  }

  public function PeriodSettingsUpdAct($id) {

    $post = $this->input->post();
    if(strtotime($post['periodfinish']) < strtotime($post['periodstart'])) {
      $this->session->set_flashdata('failed',ucfirst('period finish must not be less than period start'));
      redirect('back/periodeedit/'.$id);
    }
    if(strtotime($post['periodearly']) > strtotime($post['periodfinish'])) {
      $this->session->set_flashdata('failed',ucfirst('period early must not be less than period finish'));
      redirect('back/periodeedit/'.$id);
    }
    if(strtotime($post['periodregular']) > strtotime($post['periodfinish'])) {
      $this->session->set_flashdata('failed',ucfirst('period regular must not be less than period finish'));
      redirect('back/periodeedit/'.$id);
    }
    $action = $this->Period->PeriodSettingsUpdAct($id);
    if($action) {
      $this->session->set_flashdata('success',ucfirst('period settings successfully added'));
      redirect('back/periodsettings');
    } else if(!$action) {
      $this->session->set_flashdata('failed',ucfirst('period settings failed to be added'));
      redirect('back/periodsettings');
    }

  }

  public function PeriodActivated($id) {

    $action = $this->Period->PeriodActivated($id);
    if($action) {
      $this->session->set_flashdata('success',ucfirst('period settings successfully activated'));
      redirect('back/periodsettings');
    } else if(!$action) {
      $this->session->set_flashdata('failed',ucfirst('period settings failed to be activated'));
      redirect('back/periodsettings');
    }

  }

  public function PeriodDeactivated($id) {

    $action = $this->Period->PeriodDeactivated($id);
    if($action) {
      $this->session->set_flashdata('success',ucfirst('period settings successfully deactivated'));
      redirect('back/periodsettings');
    } else if(!$action) {
      $this->session->set_flashdata('failed',ucfirst('period settings failed to be deactivated'));
      redirect('back/periodsettings');
    }

  }

  public function PeriodDelete($id) {

    $action = $this->Period->PeriodDelete($id);
    if($action) {
      $this->session->set_flashdata('success',ucfirst('period settings successfully deleted'));
      redirect('back/periodsettings');
    } else if(!$action) {
      $this->session->set_flashdata('failed',ucfirst('period settings failed to be deleted'));
      redirect('back/periodsettings');
    }
  }
}
