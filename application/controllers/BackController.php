<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class BackController extends CI_Controller {


  public function __construct() {

    parent::__construct();
    $models = array(
      'UtilsModel' => 'Utils',
      'SettingsModel' => 'Settings',
      'CountryModel' => 'Country',
      'PeriodModel' => 'Period',
      'SpeakersModel' => 'Speakers',
      'UsersModel' => 'Users',
      'SubmissionModel' => 'Submission'
    );
    foreach($models as $model => $value) {
      $this->load->model($model,$value);
    }

  }

  public function getLayout($data, $layout = null, $layout2 = null) {

    $data['base'] = $this->Settings->BaseSettingsAll();

    $this->load->view('dashboard/_partial/_header',$data);
    $this->load->view('dashboard/_partial/_sidebar',$data);
    $this->load->view('dashboard/_partial/_topbar',$data);
    if($layout != null) {
      $this->load->view('dashboard/content/'.$layout,$data);
    }
    if($layout2 != null) {
      $this->load->view('dashboard/content/'.$layout2,$data);
    }
    $this->load->view('dashboard/_partial/_footer',$data);
    $this->load->view('dashboard/_partial/_script',$data);

  }

  public function index() {

    if(!$this->session->userdata('logged')) {
      redirect('gate/');
    }

    $data['title'] = 'Udin Seminar';
    $data['dashboard'] = 'active';
    $data['url'] = 'back/';
    $data['period'] = $this->Period->PeriodActive();
    @$data['submissiona'] = $this->Submission->CountSubmission(1,$data['period']['periode_id']);
    @$data['submissiond'] = $this->Submission->CountSubmission(2,$data['period']['periode_id']);
    @$data['submissionall'] = $this->Submission->CountSubmission(null,$data['period']['periode_id']);
    @$data['authors'] = $this->Users->CountAuthors($data['period']['periode_id']);

    // Ambil Layout
    $this->getLayout($data,'dashboard');

  }

  public function FrontSettings() {

    if(!$this->session->userdata('logged')) {
      redirect('gate/');
    }

    $data['title'] = 'Udin Seminar - Front Settings';
    $data['subtitle'] = 'Front Settings';
    $data['frontsettings'] = 'active';
    $data['url'] = 'back/frontsettingsact';

    $data['form'][] = array('type' => 'text', 'id' => 'title', 'name' => 'title', 'placeholder' => 'Front Title', 'label' => 'Front Title', 'required' => true);
    $data['form'][] = array('type' => 'text', 'id' => 'subtitle', 'name' => 'subtitle', 'placeholder' => 'Front Subtitle', 'label' => 'Front Subtitle', 'required' => true);
    $data['form'][] = array('type' => 'textarea', 'id' => 'about', 'name' => 'about', 'placeholder' => 'Front About', 'label' => 'Front About', 'required' => true);
    $data['form'][] = array('type' => 'color', 'id' => 'frontcolor', 'name' => 'frontcolor', 'placeholder' => 'Front Color', 'label' => 'Front Color', 'required' => true);
    $data['form'][] = array('type' => 'select', 'id' => 'status', 'name' => 'status', 'placeholder' => 'Status', 'label' => 'Status', 'op' => 'F','value' => $this->Utils->isActive());

    // Ambil Layout
    $this->getLayout($data,'forms');

  }

  public function FrontSettingsAct() {

    $action = $this->Settings->FrontSettingsAct();
    if($action) {
      $this->session->set_flashdata('success',ucfirst('front settings successfully set'));
      redirect('back/frontsettings');
    } else if(!$action) {
      $this->session->set_flashdata('failed',ucfirst('front settings failed to be set'));
      redirect('back/frontsettings');
    }
  }

  public function FrontHeader() {

    if(!$this->session->userdata('logged')) {
      redirect('gate/');
    }

    $data['title'] = 'Udin Seminar - Front Settings - Header';
    $data['subtitle'] = 'Front Header Settings';
    $data['frontsettings'] = 'active';
    $data['url'] = 'back/frontheaderact';

    $data['form'][] = array('type' => 'text', 'id' => 'link', 'name' => 'link', 'placeholder' => 'Submit Link', 'label' => 'Submit Link', 'required' => true);
    $data['form'][] = array('type' => 'select', 'id' => 'status', 'name' => 'status', 'placeholder' => 'Status', 'label' => 'Status', 'op' => 'F','value' => $this->Utils->isActive());
    $data['form'][] = array('type' => 'file', 'id' => 'header', 'name' => 'header', 'placeholder' => 'Front Header Logo', 'label' => 'Front Header Logo', 'required' => false);

    // Ambil Layout
    $this->getLayout($data,'forms');

  }

  public function FrontHeaderAct() {

    $action = $this->Settings->FrontHeadersAct();
    if($action) {
      $this->session->set_flashdata('success',ucfirst('front header successfully set'));
      redirect('back/frontheader');
    } else if(!$action) {
      $this->session->set_flashdata('failed',ucfirst('front header failed to be set'));
      redirect('back/frontheader');
    }
  }

  public function FrontNav() {

    if(!$this->session->userdata('logged')) {
      redirect('gate/');
    }

    $data['title'] = 'Udin Seminar - Front Settings - Nav';
    $data['subtitle'] = 'Front Nav Settings';
    $data['frontsettings'] = 'active';
    $data['url'] = 'back/frontnavact';

    $data['form'][] = array('type' => 'text', 'id' => 'navname', 'name' => 'navname', 'placeholder' => 'Nav Name', 'label' => 'Nav Name', 'required' => true);
    $data['form'][] = array('type' => 'select', 'id' => 'navparents', 'name' => 'navparents', 'placeholder' => 'Nav Parents', 'label' => 'Nav Parents', 'op' => 'N', 'value' => $this->arrayquery->arrQuery($this->Settings->FrontNavParent()), 'required' => true);
    $data['form'][] = array('type' => 'text', 'id' => 'navlink', 'name' => 'navlink', 'placeholder' => 'Nav Link', 'label' => 'Nav Link', 'required' => true);
    $data['form'][] = array('type' => 'select', 'id' => 'status', 'name' => 'status', 'placeholder' => 'Status', 'label' => 'Status', 'op' => 'F','value' => $this->Utils->isActive());
    $data['form'][] = array('type' => 'select', 'id' => 'parent', 'name' => 'parent', 'placeholder' => 'Parent', 'label' => 'Parent', 'op' => 'F','value' => $this->Utils->isParent());

    // Ambil Layout
    $this->getLayout($data,'forms');

  }

  public function FrontNavAct() {

    $action = $this->Settings->FrontNavAct();
    if($action) {
      $this->session->set_flashdata('success',ucfirst('front nav successfully set'));
      redirect('back/frontnav');
    } else if(!$action) {
      $this->session->set_flashdata('failed',ucfirst('front nav failed to be set'));
      redirect('back/frontnav');
    }
  }

  public function FrontImageSlider() {

    if(!$this->session->userdata('logged')) {
      redirect('gate/');
    }

    $data['title'] = 'Udin Seminar - Front Settings - Image SLider';
    $data['subtitle'] = 'Front Image SLider Settings';
    $data['frontsettings'] = 'active';
    $data['url'] = 'back/frontimageslideract';

    $data['form'][] = array('type' => 'text', 'id' => 'slidername', 'name' => 'slidername', 'placeholder' => 'Image Slider Name', 'label' => 'Image Slider Name', 'required' => true);
    $data['form'][] = array('type' => 'select', 'id' => 'sliderevent', 'name' => 'sliderevent', 'placeholder' => 'Image Slider Event', 'label' => 'Image Slider Event', 'op' => 'N', 'value' => $this->Period->comboPeriod(),'required' => true);
    $data['form'][] = array('type' => 'file', 'id' => 'sliderimage', 'name' => 'sliderimage', 'placeholder' => 'Image Slider', 'label' => 'Image Slider', 'required' => true);
    $data['form'][] = array('type' => 'select', 'id' => 'status', 'name' => 'status', 'placeholder' => 'Status', 'label' => 'Status', 'op' => 'F','value' => $this->Utils->isActive());

    // Ambil Layout
    $this->getLayout($data,'forms');

  }

  public function FrontImageSliderAct() {

    $action = $this->Settings->FrontImageSliderAct();
    if($action) {
      $this->session->set_flashdata('success',ucfirst('front image slider successfully set'));
      redirect('back/frontimageslider');
    } else if(!$action) {
      $this->session->set_flashdata('failed',ucfirst('front image slider failed to be set'));
      redirect('back/frontimageslider');
    }
  }

  public function FrontContent() {

    if(!$this->session->userdata('logged')) {
      redirect('gate/');
    }

    $data['title'] = 'Udin Seminar - Front Settings - Content';
    $data['subtitle'] = 'Front Content Settings';
    $data['frontsettings'] = 'active';
    $data['url'] = 'back/frontcontentact';

    $data['form'][] = array('type' => 'text', 'id' => 'title', 'name' => 'title', 'placeholder' => 'Content Title', 'label' => 'Content Title', 'required' => true);
    $data['form'][] = array('type' => 'textarea', 'id' => 'frontcontent', 'name' => 'frontcontent', 'placeholder' => 'Content', 'label' => 'Content', 'required' => true);
    $data['form'][] = array('type' => 'select', 'id' => 'status', 'name' => 'status', 'placeholder' => 'Status', 'label' => 'Status', 'op' => 'F','value' => $this->Utils->isActive());

    // Ambil Layout
    $this->getLayout($data,'forms');

  }

  public function FrontContentAct() {

    $action = $this->Settings->FrontContentAct();
    if($action) {
      $this->session->set_flashdata('success',ucfirst('front content successfully set'));
      redirect('back/frontcontent');
    } else if(!$action) {
      $this->session->set_flashdata('failed',ucfirst('front content failed to be set'));
      redirect('back/frontcontent');
    }
  }

  public function FrontContact() {

    if(!$this->session->userdata('logged')) {
      redirect('gate/');
    }

    $data['title'] = 'Udin Seminar - Front Settings - Contact';
    $data['subtitle'] = 'Front Contact Settings';
    $data['frontsettings'] = 'active';
    $data['url'] = 'back/frontcontactact';

    $data['form'][] = array('type' => 'textarea', 'id' => 'frontcontact', 'name' => 'frontcontact', 'placeholder' => 'Contact', 'label' => 'Contact', 'required' => true);
    $data['form'][] = array('type' => 'select', 'id' => 'status', 'name' => 'status', 'placeholder' => 'Status', 'label' => 'Status', 'op' => 'F','value' => $this->Utils->isActive());

    // Ambil Layout
    $this->getLayout($data,'forms');

  }

  public function FrontContactAct() {

    $action = $this->Settings->FrontContactAct();
    if($action) {
      $this->session->set_flashdata('success',ucfirst('front contact successfully set'));
      redirect('back/frontcontact');
    } else if(!$action) {
      $this->session->set_flashdata('failed',ucfirst('front contact failed to be set'));
      redirect('back/frontcontact');
    }
  }

  public function FrontAbout() {

    if(!$this->session->userdata('logged')) {
      redirect('gate/');
    }

    $data['title'] = 'Udin Seminar - Front Settings - About';
    $data['subtitle'] = 'Front About Settings';
    $data['frontsettings'] = 'active';
    $data['url'] = 'back/frontaboutact';

    $data['form'][] = array('type' => 'text', 'id' => 'abouttitle', 'name' => 'abouttitle', 'placeholder' => 'About Title', 'label' => 'About Title', 'required' => true);
    $data['form'][] = array('type' => 'textarea', 'id' => 'aboutcontent', 'name' => 'aboutcontent', 'placeholder' => 'About Content', 'label' => 'About Content', 'required' => true);
    $data['form'][] = array('type' => 'select', 'id' => 'status', 'name' => 'status', 'placeholder' => 'Status', 'label' => 'Status', 'op' => 'F','value' => $this->Utils->isActive());

    // Ambil Layout
    $this->getLayout($data,'forms');

  }

  public function FrontAboutAct() {

    $action = $this->Settings->FrontAboutAct();
    if($action) {
      $this->session->set_flashdata('success',ucfirst('front about successfully set'));
      redirect('back/frontabout');
    } else if(!$action) {
      $this->session->set_flashdata('failed',ucfirst('front about failed to be set'));
      redirect('back/frontabout');
    }
  }

  public function BaseSettings() {

    if(!$this->session->userdata('logged')) {
      redirect('gate/');
    }

    $data['title'] = 'Udin Seminar - Base Settings';
    $data['subtitle'] = 'Base Settings';
    $data['basesettings'] = 'active';
    $data['url'] = 'back/basesettingsact';

    $data['form'][] = array('type' => 'text', 'id' => 'companyname', 'name' => 'companyname', 'placeholder' => 'Company Name', 'label' => 'Company Name', 'required' => true);
    $data['form'][] = array('type' => 'email', 'id' => 'companyemail', 'name' => 'companyemail', 'placeholder' => 'Company Email', 'label' => 'Company Email', 'required' => true);
    $data['form'][] = array('type' => 'password', 'id' => 'companypassword', 'name' => 'companypassword', 'placeholder' => 'Company Password', 'label' => 'Company Password', 'required' => true);
    $data['form'][] = array('type' => 'color', 'id' => 'dashboardcolor', 'name' => 'dashboardcolor', 'placeholder' => 'Dashboard Color', 'label' => 'Dashboard Color', 'required' => true);
    $data['form'][] = array('type' => 'textarea', 'id' => 'contact', 'name' => 'contact', 'placeholder' => 'Contact Us', 'label' => 'Contact Us', 'required' => false);
    $data['form'][] = array('type' => 'select', 'id' => 'status', 'name' => 'status', 'placeholder' => 'Status', 'label' => 'Status', 'op' => 'F','value' => $this->Utils->isActive());

    $this->getLayout($data,'forms');

  }

  public function BaseSettingsAct() {

    $action = $this->Settings->BaseSettingsAct();
    if($action) {
      $this->session->set_flashdata('success',ucfirst('basic settings successfully set'));
      redirect('back/basesettings');
    } else if(!$action) {
      $this->session->set_flashdata('failed',ucfirst('basic settings failed to be set'));
      redirect('back/basesettings');
    }
  }
}


?>
