<?php

defined('BASEPATH') OR exit('No direct script access allowed');

date_default_timezone_set("Asia/Jakarta");

class FrontController extends CI_Controller {

  public function __construct() {

    parent::__construct();
    $models = array(
      'CountryModel' => 'Country',
      'UsersModel' => 'Users',
      'SettingsModel' => 'Settings',
      'PeriodModel' => 'Period',
      'SpeakersModel' => 'Speakers',
      'TransactionModel' => 'Transaction',
      'ScheduleModel' => 'Schedule'
    );
    foreach($models as $model => $value) {
      $this->load->model($model,$value);
    }

  }

  public function getLayout($data,$layout = null) {

    $data['base'] = $this->Settings->FrontSettingsAll();
    $data['header'] = $this->Settings->FrontHeaderAll();
    $data['nav'] = $this->Settings->FrontNavAll();
    $data['image'] = $this->Settings->FrontImageSliderAll();
    $data['content'] = $this->Settings->FrontContentAll();
    $data['contact'] = $this->Settings->FrontContactAll();
    $data['about'] = $this->Settings->FrontAboutAll();
    $data['period'] = $this->Period->PeriodAllActive();
    $data['periodactive'] = $this->Period->PeriodActive();
    $data['speakerscount'] = $this->Speakers->SpeakersCount($data['periodactive']['periode_id']);
    $data['speakers'] = $this->Speakers->SpeakersPeriod($data['periodactive']['periode_id']);
    $data['authors'] = $this->Users->CountAuthors($data['periodactive']['periode_id']);

    $this->load->view('landing_pagev2/_partial/_header',$data);
    if($layout != null) {
      $this->load->view('landing_pagev2/content/'.$layout,$data);
    } else if($layout == null) {
      $this->load->view('landing_pagev2/_partial/_image_slider',$data);
      $this->load->view('landing_pagev2/_partial/_register_modal',$data);
      $this->load->view('landing_pagev2/_partial/_login_modal',$data);
      $this->load->view('landing_pagev2/_partial/_aboutus_countdown',$data);
      $this->load->view('landing_pagev2/_partial/_info',$data);
      $this->load->view('landing_pagev2/_partial/_speakers',$data);
      $this->load->view('landing_pagev2/_partial/_contact',$data);
    }
    $this->load->view('landing_pagev2/_partial/_footer_area',$data);
    $this->load->view('landing_pagev2/_partial/_footer',$data);

  }

  public function index() {

    // Form Register
    $data['form'][] = array('type' => 'text', 'id' => 'firstname', 'name' => 'firstname', 'placeholder' => 'Firstname', 'label' => 'Firstname', 'required' => true);
    $data['form'][] = array('type' => 'text', 'id' => 'lastname', 'name' => 'lastname', 'placeholder' => 'Lastname', 'label' => 'Lastname', 'required' => true);
    $data['form'][] = array('type' => 'email', 'id' => 'email', 'name' => 'email', 'placeholder' => 'Email', 'label' => 'Email', 'required' => true);
    $data['form'][] = array('type' => 'select', 'id' => 'country', 'name' => 'country', 'placeholder' => 'Country', 'label' => 'Country', 'op' => 'C' ,'value' => $this->Country->CountryAll());
    $data['form'][] = array('type' => 'textarea', 'id' => 'address', 'name' => 'address', 'placeholder' => 'Address', 'label' => 'Address', 'required' => true);
    $data['form'][] = array('type' => 'text', 'id' => 'phonenumber', 'name' => 'phonenumber', 'placeholder' => 'Phonenumber', 'label' => 'Phonenumber', 'required' => true);
    $data['form'][] = array('type' => 'select', 'id' => 'type', 'name' => 'authors type', 'placeholder' => 'Authors Type', 'label' => 'Authors Type', 'op' => 'T','value' => $this->Users->AuthorsTypeCombo());

    // Form Login
    $data['login'][] = array('type' => 'email', 'id' => 'email', 'name' => 'email', 'placeholder' => 'Email', 'label' => 'Email', 'required' => true);
    $data['login'][] = array('type' => 'password', 'id' => 'password', 'name' => 'password', 'placeholder' => 'Password', 'label' => 'Password', 'required' => true);

    //$this->maintence->debug($data);

    $this->getLayout($data);

  }

  public function registerVerification($codegenerate = null) {

    if(empty($codegenerate)) {
      redirect('front');
    } else if(!empty($codegenerate)) {
      $regis = $this->Users->regisTemp($codegenerate);

      if(empty($regis['regis_token']) || $regis['is_update'] == 1 || $regis['is_delete'] == 1) {
        redirect('front');
      }

      $data['subtitle'] = 'Register Verification';
      $data['text'] = 'Please complete your registration verification';
      $data['url'] = 'front/registeract/'.$codegenerate;

      $data['form'][] = array('type' => 'text', 'id' => 'firstname', 'name' => 'firstname', 'placeholder' => 'First Name', 'label' => 'First Name', 'value' => $regis['regis_first_name'], 'required' => true, 'readonly' => true, 'size' => '6');
      $data['form'][] = array('type' => 'text', 'id' => 'lastname', 'name' => 'lastname', 'placeholder' => 'Last Name', 'label' => 'Last Name', 'value' => $regis['regis_last_name'], 'required' => true, 'readonly' => true, 'size' => '6');
      $data['form'][] = array('type' => 'select', 'id' => 'cost', 'name' => 'cost[]', 'placeholder' => 'Additional Cost', 'label' => 'Additional Cost', 'op' => 'T','value' => $this->Users->AdditionalCost(), 'size' => '12','multiple' => true);
      if($regis['regis_type'] == 1 || $regis['regis_type'] == 2) {

        if($regis['regis_type'] == 2) {

          $data['form'][] = array('type' => 'text', 'id' => 'organization', 'name' => 'organization', 'placeholder' => 'Organization', 'label' => 'Organization', 'value' => null, 'required' => true, 'readonly' => false , 'size' => '12');
          $data['form'][] = array('type' => 'text', 'id' => 'npm', 'name' => 'npm', 'placeholder' => 'Student ID', 'label' => 'Student ID', 'value' => null, 'required' => true, 'readonly' => false, 'size' => '12');
        }

        $data['form'][] = array('type' => 'text', 'id' => 'titlesubs', 'name' => 'titlesubs', 'placeholder' => 'Submission Title', 'label' => 'Submission Title', 'value' => null, 'required' => true, 'readonly' => false, 'size' => '12');
        $data['form'][] = array('type' => 'file', 'id' => 'file', 'name' => 'file', 'placeholder' => 'Choose Paper', 'label' => 'Choose Paper', 'value' => null, 'required' => true, 'readonly' => true, 'size' => '12');
      }

      $this->getLayout($data,'register');
    }
  }

  public function ScheduleAll() {

    $data['row'] = $this->Schedule->getAllSchedule();

    $this->getLayout($data,'schedule');
  }

  public function PriceAll() {

    $data['row'] = $this->Users->AuthorsPrice1();

    $this->getLayout($data,'fee');
  }

  public function AuthorsTempRegister() {

    $post = $this->input->post();
    $period = $this->Period->PeriodActive();
      $codegenerate = md5(uniqid($this->input->post('authors_name')));
      $html = "Congratulations You Have Registered To $period[periode_name] Please Verify Your Registration : <br/><br/>";
      $html .= "Your Name : ".ucfirst($post['firstname'])." ".ucfirst($post['lastname'])."<br/>";
      $html .= "Country Origin : ".$this->Country->CountryName($post['country'])."<br/>";
      $html .= "Address : ".$post['address']."<br/>";
      $html .= "Phone Number : ".$post['phonenumber']."<br/>";
      $html .= "Authors Type : ".$this->Users->AuthorsType($post['authors_type'])."<br/><br/>";
      if($post['authors_type'] == 1 || $post['authors_type'] == 2) {
        if($post['country'] == 102) {
          $money = 'IDR 150.000/page';
        } else {
          $money = '$. '.$this->utils->IDRtoDollar(150000).'/page';
        }

        $random = rand(0,9).rand(0,9).rand(0,9).rand(0,9);
        $html .= "Your Account To Submit This Paper : <br/>";
        $html .= "Login Username : ".$post['email']."<br/>";
        $html .= "Password : ".$random. "<br/><br/>";
        $html .= "Please upload your submission and obey the rule : <br/><br/>";
        $html .= "<p>The submitted paper should consist of 6 (six) to 8 (eight) pages. If it exceeds the maximum limit, an additional fee $money will be charged. One of the authors is required to attend the online conference to make a full presentation of the work.

Formatting instructions for all papers: they must be written in English and use standard single-column IOP conference proceeding templates that can be downloaded from here. Only DOC[X] files will be accepted for the review process. We will follow a single-blind review process. All papers must be original from the author’s research results and have plagiarism rates of less than 30%.</p> Please use this link to verification your data and your submission or login to ICATECH Submit Paper: <br/></br>";
      } else {
        $html .= "Please verify your indentity following this link : ";
      }
      $html .= "<h4>".base_url()."front/register/$codegenerate</h4>";

    // $this->maintence->Debug($html);

    $send = $this->utils->SendEmail($html,$this->Settings->BaseEmail(),$this->Settings->BaseEmailPass(),'ICATECH','Register '.$this->Users->AuthorsType($post['authors_type']),$post['email']);

    if($send) {

      if($post['authors_type'] == 1 || $post['authors_type'] == 2) {
        $insert_users = $this->Users->UsersRegister($random,$codegenerate);

        if(!$insert_users) {
          $this->session->set_flashdata('failed',"<script>alert('Our System Has Problem Please Try Again');</script>");
          redirect('front/');
        }
      }

      $insert = $this->Users->AuthorsRegisterTemp($codegenerate);

      if($insert) {
        $this->session->set_flashdata('success',"<script>alert('Thank You For Register and PLease Check Your Email');</script>");
        redirect('front/');
      } else if(!$insert) {
        $this->session->set_flashdata('failed',"<script>alert('Register Failed');</script>");
        redirect('front/');
      }
    } else if(!$send) {
      $this->session->set_flashdata('failed',"<script>alert('Our System Has Problem Please Try Again');</script>");
      redirect('front/');
    }
  }

  public function AuthorsRegister($codegenerate) {
    $post = $this->input->post();
    $regis = $this->Users->regisTemp($codegenerate);
    $period = $this->Period->PeriodActive();
    if(strtotime(date('Y-m-d')) <= strtotime($period['periode_earlybird'])) {
      $code = 'EB';
      $code2 = 'Early Bird';
    } else if(strtotime(date('Y-m-d')) > strtotime($period['periode_earlybird'])) {
      $code = 'R';
      $code2 = 'Regular';
    }
    $insert = $this->Users->AuthorsRegister($codegenerate,$code);
    $price = $this->Users->AuthorsPrice1($regis['regis_type'],$code);
    $total = $this->Transaction->SumTransaction($regis['regis_type'],@$post['cost'],$code);
    if($insert) {
      // Halaman Email
      if($regis['regis_type'] == 3) {
        $html = "Thank you for joining $period[periode_name] please do the payment process with a total price that has been determined for the following details: <br/>";
        $html .= "Your Name : ".ucfirst($regis['regis_first_name'])." ".ucfirst($regis['regis_last_name'])."<br/> $code2";
        $html .= "Country Origin : ".$this->Country->CountryName($regis['regis_country'])."<br/>";
        $html .= "Address : ".$regis['regis_address']."<br/>";
        $html .= "Phone Number : ".$regis['regis_phone_number']."<br/>";
        $html .= "Authors Type : ".$this->Users->AuthorsType($regis['regis_type'])."<br/><br/>";
        $html .= "<h3><center>Details Payment : <center></h3><br/>";
        if($regis['regis_country'] == '102') {
          $html .= $this->Users->AuthorsType($regis['regis_type'])." : Rp.".number_format($price['total_price'])."<br/>";
          $html .= "Additional Cost : <br/>";
          if(!empty($post['cost'])) {
            foreach($post['cost'] as $key => $value) {
              $html .= $this->Users->AdditionalCost($value)." : Rp.".number_format($this->Users->AdditionalCostPrice($value))."<br/>";
            }
          }
          $html .= "<br/><br/><h2>Your Total Bill : Rp.".number_format($total)."</h2>";

        } else if($regis['regis_country'] != '102') {
          $html .= $this->Users->AuthorsType($regis['regis_type'])." : $.".$this->utils->IDRtoDollar($price['total_price'])."<br/>";
          $html .= "Additional Cost : <br/>";
          if(!empty($post['cost'])) {
            foreach($post['cost'] as $key => $value) {
              $html .= $this->Users->AdditionalCost($value)." : $.".$this->utils->IDRtoDollar($this->Users->AdditionalCostPrice($value))."<br/>";
            }
          }
          $html .= "<br/><br/><h2>Your Total Bill : $.".$this->utils->IDRtoDollar($total)."</h2>";
          $pay = 'Payment Details - ';
        }
      } else if($regis['regis_type'] == 1 || $regis['regis_type'] == 2) {
        $html = "Thank you for submitting your submission. We will correct your submission";
        $pay = NULL;
      }

      //$this->maintence->Debug($html);

       $send = $this->utils->SendEmail($html,$this->Settings->BaseEmail(),$this->Settings->BaseEmailPass(),'ICATECH',$pay.'Register Successfully',$regis['regis_email']);

      if($send) {
        $act = $this->Users->UpdateTempRegisIfSuccess($codegenerate);
        $this->session->set_flashdata('success',"<script>alert('Thank You For Register and PLease Check Your Email');</script>");
        redirect('front/');
      } else if(!$send) {
        $act = $this->Users->UpdateTempRegisIfFailed($codegenerate);
        $this->session->set_flashdata('failed',"<script>alert('Failed To email cant send');</script>");
        redirect('front/register/'.$codegenerate);
      }
    } else if(!$insert) {
      $act = $this->Users->UpdateTempRegisIfFailed($codegenerate);
      $this->session->set_flashdata('failed',"<script>alert('Failed To registration please try again');</script>");
        redirect('front/register/'.$codegenerate);
    }
  }
}


?>
