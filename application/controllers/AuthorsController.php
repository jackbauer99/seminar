<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class AuthorsController extends CI_Controller {

  public function __construct() {

    parent::__construct();
    $models = array(
      'UtilsModel' => 'Utils',
      'SettingsModel' => 'Settings',
      'CountryModel' => 'Country',
      'PeriodModel' => 'Period',
      'SpeakersModel' => 'Speakers',
      'UsersModel' => 'Users',
      'SubmissionModel' => 'Submission'
    );
    foreach($models as $model => $value) {
      $this->load->model($model,$value);
    }

  }

  public function getLayout($data, $layout = null, $layout2 = null) {

    $data['base'] = $this->Settings->BaseSettingsAll();

    $this->load->view('dashboard/_partial/_header',$data);
    $this->load->view('dashboard/_partial/_sidebar',$data);
    $this->load->view('dashboard/_partial/_topbar',$data);
    if($layout != null) {
      $this->load->view('dashboard/content/'.$layout,$data);
    }
    if($layout2 != null) {
      $this->load->view('dashboard/content/'.$layout2,$data);
    }
    $this->load->view('dashboard/_partial/_footer',$data);
    $this->load->view('dashboard/_partial/_script',$data);

  }

  public function AuthorsType() {

    if(!$this->session->userdata('logged')) {
      redirect('gate/');
    }

    $data['title'] = 'Udin Seminar - Regis Type';
    $data['subtitle'] = 'Regis Type';
    $data['master'] = 'active';
    $data['row'] = $this->Users->AuthorsType();
    $data['url'] = 'back/authorstypeact';

    $data['table'] = $this->Users->TableAuthorsType();
    $data['id'] = $this->Users->BaseID('type_regis');
    $data['column'] = $this->Users->ColumnAuthorsType();

    $data['form'][] = array('type' => 'text', 'id' => 'name_type', 'name' => 'name_type', 'placeholder' => 'Type Name', 'label' => 'Type Name', 'required' => true);

    $this->getLayout($data,'ms_table','modal');

  }

  public function AuthorsTypeAct() {

    $action = $this->Users->AuthorsTypeAct();
    if($action) {
      $this->session->set_flashdata('success',ucfirst('regis type successfully added'));
      redirect('back/authorstype');
    } else if(!$action) {
      $this->session->set_flashdata('failed',ucfirst('regis type failed to be added'));
      redirect('back/authorstype');
    }

  }

  public function AuthorsPrice() {

    if(!$this->session->userdata('logged')) {
      redirect('gate/');
    }

    $data['title'] = 'Udin Seminar - Price Type';
    $data['subtitle'] = 'Price Type';
    $data['master'] = 'active';
    $data['row'] = $this->Users->AuthorsPrice1();
    $data['url'] = 'back/pricetypeact';

    //$this->maintence->Debug($data['row']);

    $data['table'] = $this->Users->TablePriceType();
    //$data['id'] = $this->Users->BaseID('price');
    $data['column'] = $this->Users->ColumnPriceType();

    //$this->maintence->Debug($this->Users->PriceStatus());

    $data['form'][] = array('type' => 'select', 'id' => 'authors_type', 'op' => 'F' , 'name' => 'authors_type', 'placeholder' => 'Authors Type', 'label' => 'Authors Type', 'required' => true, 'value' => $this->Users->AuthorsTypeCombo());
    $data['form'][] = array('type' => 'number', 'id' => 'total', 'name' => 'total', 'placeholder' => 'Price', 'label' => 'Price', 'required' => true);
    $data['form'][] = array('type' => 'select', 'id' => 'status_price', 'op' => 'F' , 'name' => 'status_price', 'placeholder' => 'Status Price', 'label' => 'Status Price', 'required' => true, 'value' => $this->Users->PriceStatus());

    $this->getLayout($data,'ms_table','modal');

  }

  public function AuthorsPriceAct() {

    $action = $this->Users->AuthorsPriceAct();
    if($action) {
      // if($action == 'warning') {
      //   $this->session->set_flashdata('failed',ucfirst('price type failed to be added'));
      //   redirect('back/pricetype');
      // } else if($action != 'warning'){
        $this->session->set_flashdata('success',ucfirst('price type successfully added'));
        redirect('back/pricetype');
      // /}
    } else if(!$action) {
      $this->session->set_flashdata('failed',ucfirst('price type failed to be added'));
      redirect('back/pricetype');
    }

  }

  public function AuthorsAll() {

    if(!$this->session->userdata('logged')) {
      redirect('gate/');
    }

    $data['title'] = 'Udin Seminar - Authors List';
    $data['subtitle'] = 'Authors List';
    $data['master'] = 'active';
    $data['row'] = $this->Users->AuthorsAll();


    $data['table'] = $this->Users->BaseTable();
    $data['id'] = $this->Users->BaseID('authors');
    $data['detailname'] = $this->Users->BaseName();
    $data['column'] = $this->Users->BaseColumn();
    $data['detailtable'] = $this->Users->BaseDetailTable();
    $data['detailcolumn'] = $this->Users->BaseDetailColumn();
    $data['detailrow'] = $this->Users->DetailAuthorsAll();


    $this->getLayout($data,'ms_table','modal_detail');

  }
}
