<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class AuthController extends CI_Controller {


  public function __construct() {

    parent::__construct();
    $this->load->model('AuthModel');

  }

  public function index($languange = null) {

    if($this->session->userdata('logged')) {
      redirect('back/');
    }

    $data['title'] = 'Udin Seminar - Login';

    if($languange == null || $languange == 'idn') {
      $data['subtitle'] = 'Selamat Datang Kembali';
      $data['button'] = 'Masuk';
      $data['languange'] = 'Ganti Bahasa ';
      $data['ENG'] = 'Inggris';
      $data['IDN'] = 'Indonesia';
      $data['forgot_password']['text'] = 'Lupa Password ?';
    } else if($languange == 'eng') {
      $data['subtitle'] = 'Welcome Back';
      $data['button'] = 'Login';
      $data['languange'] = 'Change Languange To ';
      $data['ENG'] = 'English';
      $data['IDN'] = 'Indonesia';
      $data['forgot_password']['text'] = 'Forgot Password ?';
    }

    $data['form'][] = array('type' => 'text', 'id' => 'username', 'name' => 'username', 'placeholder' => 'Username', 'label' => 'Username');
    $data['form'][] = array('type' => 'password', 'id' => 'password', 'name' => 'password', 'placeholder' => 'Password', 'label' => 'Password');
    //$data['form'][] = array('type' => 'checkbox', 'id' => 'remember', 'name' => 'remember', 'placeholder' => 'Remember', 'label' => 'Remember', 'value' => 1);
    $data['forgot_password']['active'] = true;

    if($data['forgot_password']['active']) {
      if($languange != null) {
          $data['forgot_password']['text'];
      } else {
        $data['forgot_password']['text'];
      }

    }

    $this->load->view('dashboard/_partial/_header',$data);
    $this->load->view('dashboard/gate/login',$data);
    $this->load->view('dashboard/_partial/_script');

  }

  public function authProcess() {

    $pevious_link = $_SERVER['HTTP_REFERER'];
    $exp = explode('/',$_SERVER['HTTP_REFERER']);

    // if($exp[5] == null || $exp[5] == 'idn') {
    //   $username_not_found = 'Username Tidak Ditemukan';
    //   $password_not_found = 'Password Tidak Ditemukan';
    // } else if($exp[5] == 'eng') {
      $password_not_found = 'Password Not Found';
      $username_not_found = 'Username Not Found';
    //}

    $post = $this->input->post();
    if($this->AuthModel->authValidation()) {
      $query = $this->AuthModel->authCheck($post['username']);
      if($post['username'] == 'foo' && $post['password'] == 'bar') {
        $session = array(
          'logged' => TRUE,
          'username' => 'admin'
        );
        $this->session->set_userdata($session);
        redirect('back/');

      } else {
        $this->session->set_flashdata('failed',"Username Not Found");
        redirect('gate/');
      }
    } else {
      $this->session->set_flashdata('failed',validation_errors());
      redirect('gate/');
    }
  }

  public function AuthorsLogin() {

    $post = $this->input->post();
    $query = $this->AuthModel->authCheck($post['email']);

    if($query) {
      if($query['users_password'] == $post['password']) {
        redirect('front/register/'.$query['url_token']);
      } else {
        $this->session->set_flashdata('failed',"<script>alert('Login Failed');</script>");
        redirect('front/');
      }
    } else {
      $this->session->set_flashdata('failed',"<script>alert('Login Failed');</script>");
      redirect('front/');
    }
  }

  public function logout() {

    $this->session->sess_destroy();
    redirect('gate/');

  }
}


?>
