<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class SpeakersController extends CI_Controller {

  public function __construct() {

    parent::__construct();
    $models = array(
      'UtilsModel' => 'Utils',
      'SettingsModel' => 'Settings',
      'CountryModel' => 'Country',
      'PeriodModel' => 'Period',
      'SpeakersModel' => 'Speakers',
      'UsersModel' => 'Users',
      'SubmissionModel' => 'Submission'
    );
    foreach($models as $model => $value) {
      $this->load->model($model,$value);
    }
  }

  public function getLayout($data, $layout = null, $layout2 = null) {

    $data['base'] = $this->Settings->BaseSettingsAll();

    $this->load->view('dashboard/_partial/_header',$data);
    $this->load->view('dashboard/_partial/_sidebar',$data);
    $this->load->view('dashboard/_partial/_topbar',$data);
    if($layout != null) {
      $this->load->view('dashboard/content/'.$layout,$data);
    }
    if($layout2 != null) {
      $this->load->view('dashboard/content/'.$layout2,$data);
    }
    $this->load->view('dashboard/_partial/_footer',$data);
    $this->load->view('dashboard/_partial/_script',$data);

  }

  public function SpeakersSettings() {

    if(!$this->session->userdata('logged')) {
      redirect('gate/');
    }

    $data['title'] = 'Udin Seminar - Speakers Settings';
    $data['subtitle'] = 'Speakers Settings';
    $data['eventsettings'] = 'active';
    $data['row'] = $this->Speakers->SpeakersAll();
    $data['url'] = 'back/speakersact';
    $data['urlact'] = 'back/speakersactivated/';
    $data['urldeact'] = 'back/speakersdeactivated/';
    $data['urldelete'] = 'back/speakersdelete/';

    $data['table'] = $this->Speakers->table();
    $data['id'] = $this->Speakers->id();
    $data['column'] = $this->Speakers->column();

    //$this->maintence->Debug($data['column']);

    $data['form'][] = array('type' => 'text', 'id' => 'speakersname', 'name' => 'speakersname', 'placeholder' => 'Speakers Name', 'label' => 'Speakers Name', 'required' => true);
    $data['form'][] = array('type' => 'textarea', 'id' => 'speakersjob', 'name' => 'speakersjob', 'placeholder' => 'Speakers Job', 'label' => 'Speakers Job', 'required' => true);
    $data['form'][] = array('type' => 'file', 'id' => 'speakersimage', 'name' => 'speakersimage', 'placeholder' => 'Speakers Image', 'label' => 'Speakers Image', 'required' => true);
    $data['form'][] = array('type' => 'select', 'id' => 'status', 'name' => 'status', 'placeholder' => 'Status', 'label' => 'Status', 'op' => 'F','value' => $this->Utils->isActive());

    $this->getLayout($data,'table','modal');

  }

  public function SpeakersSettingsAct() {

    $action = $this->Speakers->SpeakersSettingsAct();
    if($action) {
      $this->session->set_flashdata('success',ucfirst('speakers successfully added'));
      redirect('back/speakers');
    } else if(!$action) {
      $this->session->set_flashdata('failed',ucfirst('speakers failed to be added'));
      redirect('back/speakers');
    }

  }
}
