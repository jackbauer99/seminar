<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class ScheduleController extends CI_Controller {

  public function __construct() {
    parent::__construct();
    $models = array(
      'UtilsModel' => 'Utils',
      'SettingsModel' => 'Settings',
      'CountryModel' => 'Country',
      'PeriodModel' => 'Period',
      'SpeakersModel' => 'Speakers',
      'UsersModel' => 'Users',
      'SubmissionModel' => 'Submission',
      'TransactionModel' => 'Transaction',
      'ScheduleModel' => 'Schedule'
    );
    foreach($models as $model => $value) {
      $this->load->model($model,$value);
    }
  }

  public function getLayout($data, $layout = null, $layout2 = null, $layout3 = null) {

    $data['base'] = $this->Settings->BaseSettingsAll();

    $this->load->view('dashboard/_partial/_header',$data);
    $this->load->view('dashboard/_partial/_sidebar',$data);
    $this->load->view('dashboard/_partial/_topbar',$data);
    if($layout != null) {
      $this->load->view('dashboard/content/'.$layout,$data);
    }
    if($layout2 != null) {
      $this->load->view('dashboard/content/'.$layout2,$data);
    }
    if($layout3 != null) {
      $this->load->view('dashboard/content/'.$layout3,$data);
    }
    $this->load->view('dashboard/_partial/_footer',$data);
    $this->load->view('dashboard/_partial/_script',$data);

  }

  public function ScheduleAll() {
    $period = $this->Period->PeriodActive();
    $data['title'] = 'Udin Seminar - Schedule Settings';
    $data['subtitle'] = 'Schedule Settings';
    $data['eventsettings'] = 'active';
    $data['row'] = $this->Schedule->getAllSchedule($period['periode_id']);
    $data['urladd'] = 'back/scheduleadd';
    // $data['editable'] = true;
    // $data['url'] = 'back/periodsettingsact';
    // $data['urlact'] = 'back/periodactivated/';
    // $data['urldeact'] = 'back/perioddeactivated/';
    // $data['urldelete'] = 'back/perioddelete/';

    $data['table'] = $this->Schedule->BaseTable();
    $data['id'] = $this->Schedule->BaseID();
    $data['column'] = $this->Schedule->BaseColumn();

    $this->getLayout($data,'ms_table');

  }

  public function ScheduleAdd() {

    $data['title'] = 'Udin Seminar - Schedule Settings';
    $data['subtitle'] = 'Schedule Settings';
    $data['eventsettings'] = 'active';
    $data['url'] = 'back/scheduleact';

    $data['form'][] = array('type' => 'text', 'id' => 'schedule_name', 'name' => 'schedule_name', 'placeholder' => 'Schedule Name', 'label' => 'Schedule Name', 'required' => true);
    $data['form'][] = array('type' => 'date', 'id' => 'schedule_time', 'name' => 'schedule_time', 'placeholder' => 'Schedule Time', 'label' => 'Schedule Time', 'required' => true);

    $this->getLayout($data,'forms');

  }

  public function ScheduleAddAct() {

    $post = $this->input->post();
    $period = $this->Period->PeriodActive();
    if(strtotime($post['schedule_time']) < strtotime($period['periode_start'])) {
      $this->session->set_flashdata('failed',ucfirst('schedule date cant be less to period date'));
      redirect('back/scheduleadd');
    }
    else if(strtotime($post['schedule_time']) > strtotime($period['periode_finish'])) {
      $this->session->set_flashdata('failed',ucfirst('schedule date should not be more period date'));
      redirect('back/scheduleadd');
    }

    $action = $this->Schedule->ScheduleAddAct();

    if($action) {
      $this->session->set_flashdata('success',ucfirst('schedule successfully add'));
      redirect('back/schedule');
    } else if(!$action) {
      $this->session->set_flashdata('failed',ucfirst('schedule failed to add'));
      redirect('back/scheduleadd');
    }
  }

}
