<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class SubmissionController extends CI_Controller {

  public function __construct() {

    parent::__construct();
    $models = array(
      'UtilsModel' => 'Utils',
      'SettingsModel' => 'Settings',
      'CountryModel' => 'Country',
      'PeriodModel' => 'Period',
      'SpeakersModel' => 'Speakers',
      'UsersModel' => 'Users',
      'SubmissionModel' => 'Submission',
      'TransactionModel' => 'Transaction'
    );
    foreach($models as $model => $value) {
      $this->load->model($model,$value);
    }

  }

  public function getLayout($data, $layout = null, $layout2 = null) {

    $data['base'] = $this->Settings->BaseSettingsAll();

    $this->load->view('dashboard/_partial/_header',$data);
    $this->load->view('dashboard/_partial/_sidebar',$data);
    $this->load->view('dashboard/_partial/_topbar',$data);
    if($layout != null) {
      $this->load->view('dashboard/content/'.$layout,$data);
    }
    if($layout2 != null) {
      $this->load->view('dashboard/content/'.$layout2,$data);
    }
    $this->load->view('dashboard/_partial/_footer',$data);
    $this->load->view('dashboard/_partial/_script',$data);

  }

  public function SubmissionAll() {

    if(!$this->session->userdata('logged')) {
      redirect('gate/');
    }

    $data['title'] = 'Udin Seminar - Authors List';
    $data['subtitle'] = 'Authors List';
    $data['master'] = 'active';
    $data['urlact'] = 'back/submissionapprove/';
    $data['urldeact'] = 'back/submissiondenied/';
    $data['row'] = $this->Submission->SubmissionAll();
    $data['table'] = $this->Submission->BaseTable();
    $data['column'] = $this->Submission->BaseColumn();
    $data['id'] = $this->Submission->BaseID();

    $this->getLayout($data,'file_table');

  }

  public function SubmissionApprove($id) {

    $submission = $this->Submission->getSubmission($id);
    $authors = $this->Users->getAuthors($submission['submission_authors']);
    $period = $this->Period->PeriodActive();
    $transaction = $this->Transaction->getTransactionAuthors($authors['authors_id']);

    if(strtotime(date('Y-m-d')) <= strtotime($period['periode_earlybird'])) {
      $code = 'EB';
      $code2 = 'Early Bird';
    } else if(strtotime(date('Y-m-d')) > strtotime($period['periode_earlybird'])) {
      $code = 'R';
      $code2 = 'Regular';
    }

    $price = $this->Users->AuthorsPrice1($authors['authors_type'],$code);
    $update = $this->Submission->SubmissionApprove($id);

    if($update) {
      $html = "<h3>Your Submission $submission[submission_title] has been approved</h3><br/><br/>";
      $html .= "Thank you for joining $period[periode_name] please do the payment process with a total price that has been determined for the following details: <br/>";
      $html .= "Your Name : ".ucfirst($authors['authors_first_name'])." ".ucfirst($authors['authors_last_name'])."<br/>";
      $html .= "Country Origin : ".$this->Country->CountryName($authors['authors_country'])."<br/>";
      $html .= "Address : ".$authors['authors_address']."<br/>";
      $html .= "Phone Number : ".$authors['authors_phone_number']."<br/>";
      $html .= "Authors Type : ".$this->Users->AuthorsType($authors['authors_type'])."<br/><br/>";
      $html .= "<h3><center>Details Payment : <center></h3><br/>";
      if($authors['authors_country'] == '102') {
        $html .= $this->Users->AuthorsType($authors['authors_type'])." : Rp.".number_format($price['total_price'])."<br/>";
        $html .= "Additional Cost : <br/>";
        if($transaction['transaction_additional'] != NULL) {
          if($transaction['transaction_additional'] == '0,1') {
            $exp = explode(',',$transaction['transaction_additional']);
            foreach($exp as $key => $value) {
              $html .= $this->Users->AdditionalCost($value)." : Rp.".number_format($this->Users->AdditionalCostPrice($value))."<br/>";
            }
          } else if($transaction['transaction_additional'] != '0,1') {
              $html .= $this->Users->AdditionalCost($transaction['transaction_additional'])." : Rp.".number_format($this->Users->AdditionalCostPrice($transaction['transaction_additional']))."<br/>";
          }
        }
        $html .= "<br/><br/><h2>Your Total Bill : Rp.".number_format($transaction['transaction_total'])."</h2>";

      } else if($authors['authors_country'] != '102') {
        $html .= $this->Users->AuthorsType($authors['authors_type'])." : $.".$this->utils->IDRtoDollar($price['total_price'])."<br/>";
        $html .= "Additional Cost : <br/>";
        if($transaction['transaction_additional'] != NULL) {
          if($transaction['transaction_additional'] == '0,1') {
            $exp = explode(',',$transaction['transaction_additional']);
            foreach($exp as $key => $value) {
              $html .= $this->Users->AdditionalCost($value)." : $.".$this->utils->IDRtoDollar($this->Users->AdditionalCostPrice($value))."<br/>";
            }
          } else if($transaction['transaction_additional'] != '0,1') {
              $html .= $this->Users->AdditionalCost($transaction['transaction_additional'])." : $.".$this->utils->IDRtoDollar($this->Users->AdditionalCostPrice($transaction['transaction_additional']))."<br/>";
          }
        }
        $html .= "<br/><br/><h2>Your Total Bill : $.".$this->utils->IDRtoDollar($transaction['transaction_total'])."</h2>";
      }

      //$this->maintence->Debug($html);

      $send = $this->utils->SendEmail($html,$this->Settings->BaseEmail(),$this->Settings->BaseEmailPass(),'ICATECH','Submission Approve - Payment Details',$authors['authors_email']);

      if($send) {
        $this->session->set_flashdata('success',ucfirst('submission approved'));
        redirect('back/submission');
      } else if(!$send) {
        $this->session->set_flashdata('failed',ucfirst('submission approved but email not send'));
        redirect('back/submission');
      }
    } else if(!$update) {
        $this->session->set_flashdata('failed',ucfirst("submission can't approved"));
        redirect('back/submission');
    }
  }

  public function SubmissionDenied($id) {

    $submission = $this->Submission->getSubmission($id);
    $authors_before = $this->Users->getAuthors($submission['submission_authors']);
    $period = $this->Period->PeriodActive();
    $transaction_before = $this->Transaction->getTransactionAuthors($authors_before['authors_id']);

    if(strtotime(date('Y-m-d')) <= strtotime($period['periode_earlybird'])) {
      $code = 'EB';
      $code2 = 'Early Bird';
    } else if(strtotime(date('Y-m-d')) > strtotime($period['periode_earlybird'])) {
      $code = 'R';
      $code2 = 'Regular';
    }

    $price_before = $this->Users->AuthorsPrice1($authors_before['authors_type'],$code);

    $update_authors = $this->Users->AuthorsUpdateAct($submission['submission_authors']);
    $authors_after = $this->Users->getAuthors($submission['submission_authors']);
    $price_after = $this->Users->AuthorsPrice1($authors_after['authors_type'],$code);

    $update_transaction = $this->Transaction->UpdateTransaction($submission['submission_authors'],3,$price_before['total_price'],$price_after['total_price'],$transaction_before['transaction_total']);

    $transaction_after = $this->Transaction->getTransactionAuthors($authors_after['authors_id']);

    //$this->maintence->Debug($cek);
    $update = $this->Submission->SubmissionDenied($id);

    if($update) {
      $html = "<h3>Your Submission $submission[submission_title] has been denied because does not meet the criteria</h3><br/><br/>";
      $html .= "Thank you for joining $period[periode_name] please do the payment process with a total price that has been determined for the following details: <br/>";
      $html .= "Your Name : ".ucfirst($authors_after['authors_first_name'])." ".ucfirst($authors_after['authors_last_name'])."<br/>";
      $html .= "Country Origin : ".$this->Country->CountryName($authors_after['authors_country'])."<br/>";
      $html .= "Address : ".$authors_after['authors_address']."<br/>";
      $html .= "Phone Number : ".$authors_after['authors_phone_number']."<br/>";
      $html .= "Authors Type : ".$this->Users->AuthorsType($authors_after['authors_type'])."<br/><br/>";
      $html .= "<h3><center>Details Payment : <center></h3><br/>";
      if($authors_after['authors_country'] == '102') {
        $html .= $this->Users->AuthorsType($authors_after['authors_type'])." : Rp.".number_format($price_after['total_price'])."<br/>";
        $html .= "Additional Cost : <br/>";
        if($transaction_after['transaction_additional'] != NULL) {
          if($transaction_after['transaction_additional'] == '0,1') {
            $exp = explode(',',$transaction_after['transaction_additional']);
            foreach($exp as $key => $value) {
              $html .= $this->Users->AdditionalCost($value)." : Rp.".number_format($this->Users->AdditionalCostPrice($value))."<br/>";
            }
          } else if($transaction_after['transaction_additional'] != '0,1') {
              $html .= $this->Users->AdditionalCost($transaction_after['transaction_additional'])." : Rp.".number_format($this->Users->AdditionalCostPrice($transaction_after['transaction_additional']))."<br/>";
          }
        }
        $html .= "<br/><br/><h2>Your Total Bill : Rp.".number_format($transaction_after['transaction_total'])."</h2>";

      } else if($authors_after['authors_country'] != '102') {
        $html .= $this->Users->AuthorsType($authors_after['authors_type'])." : $.".$this->utils->IDRtoDollar($price_after['total_price'])."<br/>";
        $html .= "Additional Cost : <br/>";
        if($transaction_after['transaction_additional'] != NULL) {
          if($transaction_after['transaction_additional'] == '0,1') {
            $exp = explode(',',$transaction_after['transaction_additional']);
            foreach($exp as $key => $value) {
              $html .= $this->Users->AdditionalCost($value)." : $.".$this->utils->IDRtoDollar($this->Users->AdditionalCostPrice($value))."<br/>";
            }
          } else if($transaction_after['transaction_additional'] != '0,1') {
              $html .= $this->Users->AdditionalCost($transaction_after['transaction_additional'])." : $.".$this->utils->IDRtoDollar($this->Users->AdditionalCostPrice($transaction_after['transaction_additional']))."<br/>";
          }
        }
        $html .= "<br/><br/><h2>Your Total Bill : $.".$this->utils->IDRtoDollar($transaction_after['transaction_total'])."</h2>";
      }

      //$this->maintence->Debug($html);

      $send = $this->utils->SendEmail($html,$this->Settings->BaseEmail(),$this->Settings->BaseEmailPass(),'ICATECH','Submission Denied - Payment Details',$authors_after['authors_email']);

      if($send) {
        $this->session->set_flashdata('success',ucfirst('submission denied'));
        redirect('back/submission');
      } else if(!$send) {
        $this->session->set_flashdata('failed',ucfirst('submission denied but email not send'));
        redirect('back/submission');
      }
    } else if(!$update) {
        $this->session->set_flashdata('failed',ucfirst("submission can't approved"));
        redirect('back/submission');
    }
  }
}
