<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'FrontController';

// Front
$route['front'] = 'FrontController/index';
$route['front/tempregistact'] = 'FrontController/AuthorsTempRegister';
$route['front/register/(:any)'] = 'FrontController/registerVerification/$1';
$route['front/registeract/(:any)'] = 'FrontController/AuthorsRegister/$1';
$route['front/loginact'] = 'AuthController/AuthorsLogin';
$route['front/schedule'] = 'FrontController/ScheduleAll';
$route['front/fee'] = 'FrontController/PriceAll';
$route['back'] = 'BackController/index';
$route['migrate'] = 'Migrate';

// Gate Login
$route['gate'] = 'AuthController/index/eng';
$route['gate/eng'] = 'AuthController/index/eng';
$route['gate/idn'] = 'AuthController/index/idn';
$route['gate/auth'] = 'AuthController/authProcess';

// Front Settings
$route['back/frontsettings'] = 'BackController/FrontSettings';
$route['back/frontheader'] = 'BackController/FrontHeader';
$route['back/frontnav'] = 'BackController/FrontNav';
$route['back/frontimageslider'] = 'BackController/FrontImageSlider';
$route['back/frontcontent'] = 'BackController/FrontContent';
$route['back/frontcontact'] = 'BackController/FrontContact';
$route['back/frontabout'] = 'BackController/FrontAbout';
$route['back/frontsettingsact'] = 'BackController/FrontSettingsAct';
$route['back/frontheaderact'] = 'BackController/FrontHeaderAct';
$route['back/frontnavact'] = 'BackController/FrontNavAct';
$route['back/frontimageslideract'] = 'BackController/FrontImageSliderAct';
$route['back/frontcontentact'] = 'BackController/FrontContentAct';
$route['back/frontcontactact'] = 'BackController/FrontContactAct';
$route['back/frontaboutact'] = 'BackController/FrontAboutAct';

// Base Settings
$route['back/basesettings'] = 'BackController/BaseSettings';
$route['back/basesettingsact'] = 'BackController/BaseSettingsAct';
$route['gate/auth/logout'] = 'AuthController/logout';

// Period
$route['back/periodsettings'] = 'PeriodController/PeriodSettings';
$route['back/periodsettingsact'] = 'PeriodController/PeriodSettingsAct';
$route['back/periodactivated/(:any)'] = 'PeriodController/PeriodActivated/$1';
$route['back/perioddeactivated/(:any)'] = 'PeriodController/PeriodDeactivated/$1';
$route['back/perioddelete/(:any)'] = 'PeriodController/PeriodDelete/$1';
$route['back/periodeedit/(:any)'] = 'PeriodController/PeriodEdit/$1';
$route['back/periodeupdate/(:any)'] = 'PeriodController/PeriodSettingsUpdAct/$1';

// Speakers
$route['back/speakers'] = 'SpeakersController/SpeakersSettings';
$route['back/speakersact'] = 'SpeakersController/SpeakersSettingsAct';

// Schedule
$route['back/schedule'] = 'ScheduleController/ScheduleAll';
$route['back/scheduleadd'] = 'ScheduleController/ScheduleAdd';
$route['back/scheduleact'] = 'ScheduleController/ScheduleAddAct';

// Master
$route['back/authorstype'] = 'AuthorsController/AuthorsType';
$route['back/authorstypeact'] = 'AuthorsController/AuthorsTypeAct';
$route['back/pricetypeact'] = 'AuthorsController/AuthorsPriceAct';
$route['back/pricetype'] = 'AuthorsController/AuthorsPrice';
$route['back/authors'] = 'AuthorsController/AuthorsAll';
$route['back/submission'] = 'SubmissionController/SubmissionAll';
$route['back/submissionapprove/(:any)'] = 'SubmissionController/SubmissionApprove/$1';
$route['back/submissiondenied/(:any)'] = 'SubmissionController/SubmissionDenied/$1';


// 404 Error
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
